package io.xtrd.samples.common;

public class Connector {
    public enum Status {
        UNDEFINED(-1),
        DISABLED(0),
        ACTIVE(1),
        DELETED(2);

        private int value;

        Status(int value) {
            this.value = value;
        }

        public static Status fromInt(int value) {
            switch (value) {
                case 0:
                    return DISABLED;
                case 1:
                    return ACTIVE;
                case 2:
                    return DELETED;
                default:
                    return UNDEFINED;
            }
        }

        public int getIntValue() {
            return value;
        }
    }

    private int id;
    private String name;
    private String description;
    private Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
