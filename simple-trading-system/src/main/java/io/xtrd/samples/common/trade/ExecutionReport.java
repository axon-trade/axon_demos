package io.xtrd.samples.common.trade;


import io.xtrd.samples.common.md.Asset;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import org.joda.time.DateTime;

public class ExecutionReport {
    private int id;
    private long orderId;
    private int originalId;
    private String secondaryOrderId;
    private Symbol symbol;
    private Exchange exchange;
    private Order.Status status;
    private double lastPrice;
    private double lastSize;
    private double filledSize;
    private Asset commissionAsset;
    private double commissionSize;
    private DateTime receiveTime;
    private DateTime transactionTime;
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOriginalId() {
        return originalId;
    }

    public void setOriginalId(int originalId) {
        this.originalId = originalId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getSecondaryOrderId() {
        return secondaryOrderId;
    }

    public void setSecondaryOrderId(String secondaryOrderId) {
        this.secondaryOrderId = secondaryOrderId;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public Order.Status getStatus() {
        return status;
    }

    public void setStatus(Order.Status status) {
        this.status = status;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public double getLastSize() {
        return lastSize;
    }

    public void setLastSize(double lastSize) {
        this.lastSize = lastSize;
    }

    public DateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(DateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Asset getCommissionAsset() {
        return commissionAsset;
    }

    public void setCommissionAsset(Asset commissionAsset) {
        this.commissionAsset = commissionAsset;
    }

    public double getCommissionSize() {
        return commissionSize;
    }

    public void setCommissionSize(double commissionSize) {
        this.commissionSize = commissionSize;
    }

    public DateTime getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(DateTime transactionTime) {
        this.transactionTime = transactionTime;
    }

    public double getFilledSize() {
        return filledSize;
    }

    public void setFilledSize(double filledSize) {
        this.filledSize = filledSize;
    }

    @Override
    public String toString() {
        return new StringBuilder("[ExecutionReport. Order ID: ")
        .append(id).append("/").append(orderId)
        .append(", Status: ").append(status)
        .append("]")
        .toString();
    }
}
