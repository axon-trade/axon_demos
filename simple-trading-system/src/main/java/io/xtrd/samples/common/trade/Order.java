package io.xtrd.samples.common.trade;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import org.joda.time.DateTime;

public class Order {
    public enum Type {UNDEFINED, MARKET, LIMIT}
    public enum Side {UNDEFINED, BUY, SELL}
    public enum Status {UNDEFINED, PENDING_NEW, NEW, PARTIALLY_FILLED, FILLED, CANCELED, REJECTED, PENDING_REPLACE}
    public enum TimeInForce {GOOD_TILL_CANCEL, FILL_OR_KILL, IMMEDIATE_OR_CANCEL}
    public enum ExecType {TRADE, CANCEL, REPLACE}

    private int id;
    private long orderId;
    private String secondaryOrderId;
    private int originalId;
    private int loginId;
    private Symbol symbol;
    private Exchange exchange;
    private Type type;
    private Side side;
    private DateTime createTime;
    private DateTime lastUpdateTime;
    private double price;
    private double size;
    private Status status;
    private double filledSize;
    private TimeInForce timeInForce;
    private ExecType execType;

    public Order(ExecType execType) {
        this.execType = execType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getSecondaryOrderId() {
        return secondaryOrderId;
    }

    public void setSecondaryOrderId(String secondaryOrderId) {
        this.secondaryOrderId = secondaryOrderId;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public DateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(DateTime createTime) {
        this.createTime = createTime;
    }

    public DateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(DateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public double getFilledSize() {
        return filledSize;
    }

    public void setFilledSize(double filledSize) {
        this.filledSize = filledSize;
    }

    public TimeInForce getTimeInForce() {
        return timeInForce;
    }

    public void setTimeInForce(TimeInForce timeInForce) {
        this.timeInForce = timeInForce;
    }

    public ExecType getExecType() {
        return execType;
    }

    public void setExecType(ExecType execType) {
        this.execType = execType;
    }

    public int getOriginalId() {
        return originalId;
    }

    public void setOriginalId(int originalId) {
        this.originalId = originalId;
    }

    public Order copy() {
        Order result = new Order(execType);
        result.id = this.id;
        result.orderId = this.orderId;
        result.secondaryOrderId = this.secondaryOrderId;
        result.originalId = this.originalId;
        result.loginId = this.loginId;
        result.symbol = this.symbol;
        result.exchange = this.exchange;
        result.type = this.type;
        result.side = this.side;
        result.createTime = new DateTime(this.createTime);
        result.lastUpdateTime = new DateTime(this.lastUpdateTime);
        result.price = this.price;
        result.size = this.size;
        result.filledSize = this.filledSize;
        result.timeInForce = this.timeInForce;

        return result;
    }
}
