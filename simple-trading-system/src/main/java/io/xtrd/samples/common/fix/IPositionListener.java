package io.xtrd.samples.common.fix;

import io.xtrd.samples.common.trade.Position;

@FunctionalInterface
public interface IPositionListener {
    void onEvent(Position position);
}
