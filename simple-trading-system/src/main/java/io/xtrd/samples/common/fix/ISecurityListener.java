package io.xtrd.samples.common.fix;

@FunctionalInterface
public interface ISecurityListener {
    void onEvent(SecurityListEvent event);
}
