package io.xtrd.samples.common.fix;

import io.xtrd.samples.common.md.Symbol;

public class SecurityListEvent {
    private long requestId;
    private Symbol symbol;
    private int totalSymbols;
    private boolean lastEvent;

    public SecurityListEvent(long requestId, Symbol symbol, int totalSymbols, boolean lastEvent) {
        this.requestId = requestId;
        this.symbol = symbol;
        this.totalSymbols = totalSymbols;
        this.lastEvent = lastEvent;
    }

    public long getRequestId() {
        return requestId;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public int getTotalSymbols() {
        return totalSymbols;
    }

    public boolean isLastEvent() {
        return lastEvent;
    }
}
