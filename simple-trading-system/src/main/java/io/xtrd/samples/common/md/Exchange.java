package io.xtrd.samples.common.md;

public class Exchange {
    private final int id;
    private final String name;
    private final String fullName;

    protected Exchange(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.fullName = builder.fullName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String toString() {
        return new StringBuilder("[Exchange. Name: ").append(name).append("]").toString();
    }

    public static class Builder {
        int id;
        String name;
        String fullName;

        public Builder id(int id) {
            this.id = id;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder fullName(String fullName) {
            this.fullName = fullName;

            return this;
        }

        public Exchange build() {
            return new Exchange(this);
        }
    }
}
