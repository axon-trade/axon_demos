package io.xtrd.samples.common.fix;

public class ConnectionStatusEvent {
    public enum Status {
        CONNECTED,
        DISCONNECTED
    }

    private int sessionId;
    private Status status;

    public ConnectionStatusEvent(int sessionId, Status status) {
        this.sessionId = sessionId;
        this.status = status;
    }

    public int getSessionId() {
        return sessionId;
    }

    public Status getStatus() {
        return status;
    }
}
