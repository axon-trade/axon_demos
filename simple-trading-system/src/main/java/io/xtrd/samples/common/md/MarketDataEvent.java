package io.xtrd.samples.common.md;

public class MarketDataEvent {
    public enum UpdateType {UNDEFINED, NEW, UPDATE, DELETE, RESET}
    public enum Side {UNDEFINED, BID, ASK, TRADE}

    private final int symbolId;
    private final UpdateType updateType;
    private final Side side;
    private final long price;
    private final long size;
    private final long timestamp;

    public MarketDataEvent(int symbolId, UpdateType updateType, Side side, long price, long size, long timestamp) {
        this.symbolId = symbolId;
        this.updateType = updateType;
        this.side = side;
        this.price = price;
        this.size = size;
        this.timestamp = timestamp;
    }

    public int getSymbolId() {
        return symbolId;
    }

    public UpdateType getUpdateType() {
        return updateType;
    }

    public Side getSide() {
        return side;
    }

    public long getPrice() {
        return price;
    }

    public long getSize() {
        return size;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
