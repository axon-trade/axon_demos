package io.xtrd.samples.common.md;

public class Asset {
    private int id;
    private String name;

    public Asset(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Asset(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
