package io.xtrd.samples.common.trade;

import io.xtrd.samples.common.md.Asset;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Login;

public class Position {
    private Login login;
    private Asset asset;
    private Exchange exchange;
    private double size;

    public Position(Login login, Asset asset, Exchange exchange, double size) {
        this.login = login;
        this.asset = asset;
        this.exchange = exchange;
        this.size = size;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
