package io.xtrd.samples.common.fix;

@FunctionalInterface
public interface IConnectionStatusListener {
    void onEvent(ConnectionStatusEvent event);
}
