package io.xtrd.samples.common.fix;

@FunctionalInterface
public interface ITradingSessionStatusListener {
    void onEvent(TradingSessionStatusEvent event);
}
