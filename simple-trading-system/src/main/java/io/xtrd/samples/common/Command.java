package io.xtrd.samples.common;

public class Command {
    protected final long requestId;
    protected final int sessionId;

    public Command(long requestId, int sessionId) {
        this.requestId = requestId;
        this.sessionId = sessionId;
    }

    public long getRequestId() {
        return requestId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public String toString() {
        return "[Command. Request ID: " + requestId + "]";
    }
}
