package io.xtrd.samples.common.fix;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Account;

public class TradingSessionStatusEvent {
    public enum Status {OPEN, CLOSED}

    private final Status status;
    private final Account account;
    private final Exchange exchange;
    private final String text;

    public TradingSessionStatusEvent(Account account, Exchange exchange, Status status, String text) {
        this.account = account;
        this.exchange = exchange;
        this.status = status;
        this.text = text;
    }

    public TradingSessionStatusEvent(Account account, Exchange exchange, Status status) {
        this.account = account;
        this.exchange = exchange;
        this.status = status;
        this.text = "";
    }

    public Status getStatus() {
        return status;
    }

    public Account getAccount() {
        return account;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public String getText() {
        return text;
    }
}
