package io.xtrd.samples.common.fix;

import io.xtrd.samples.common.trade.ExecutionReport;

@FunctionalInterface
public interface IExecutionReportListener {
    void onEvent(ExecutionReport report);
}
