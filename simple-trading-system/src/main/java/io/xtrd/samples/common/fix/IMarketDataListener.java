package io.xtrd.samples.common.fix;

import io.xtrd.samples.common.md.MarketDataEvent;

@FunctionalInterface
public interface IMarketDataListener {
    void onEvent(MarketDataEvent events[]);
}
