package io.xtrd.samples.common.md;

public class Symbol {
    private int id;
    private final String name;
    private final int pricePower;
    private final int sizePower;
    private final long priceFactor;
    private final long sizeFactor;
    private final double minOrderSize;
    private final Exchange exchange;

    protected Symbol(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.exchange = builder.exchange;
        this.pricePower = builder.pricePower;
        this.priceFactor = (long) Math.pow(10, this.pricePower);
        this.sizePower = builder.sizePower;
        this.sizeFactor = (long) Math.pow(10, this.sizePower);
        this.minOrderSize = builder.minOrderSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getPricePower() {
        return pricePower;
    }

    public int getSizePower() {
        return sizePower;
    }

    public long getPriceFactor() {
        return priceFactor;
    }

    public long getSizeFactor() {
        return sizeFactor;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public double getMinOrderSize() {
        return minOrderSize;
    }

    public static long castToLong(double value, long factor) {
        return (long) Math.floor(value * factor + 0.5);
    }

    public static double castToDouble(long value, long factor) {
        return value / (double) factor;
    }


    @Override
    public String toString() {
        return new StringBuilder("[Symbol. Name: ").append(name).append(", Exchange: ").append(exchange).append("]").toString();
    }

    public static class Builder {
        private int id;
        private String name;
        private int pricePower;
        private int sizePower;
        private double minOrderSize;
        private Exchange exchange;

        public Builder id(int id) {
            this.id = id;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder exchange(Exchange exchange) {
            this.exchange = exchange;

            return this;
        }

        public Builder pricePower(int pricePower) {
            this.pricePower = pricePower;

            return this;
        }

        public Builder sizePower(int sizePower) {
            this.sizePower = sizePower;

            return this;
        }

        public Builder minOrderSize(double minOrderSize) {
            this.minOrderSize = minOrderSize;

            return this;
        }

        public Symbol build() {
            return new Symbol(this);
        }
    }
}
