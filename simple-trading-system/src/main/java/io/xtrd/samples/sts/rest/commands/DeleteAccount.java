package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.Command;

import java.util.function.Consumer;

public class DeleteAccount extends Command {
    private Consumer<Boolean>consumer;

    public DeleteAccount(long requestId, Consumer<Boolean> consumer) {
        super(requestId, -1);
        this.consumer = consumer;
    }

    public Consumer<Boolean> getConsumer() {
        return consumer;
    }
}
