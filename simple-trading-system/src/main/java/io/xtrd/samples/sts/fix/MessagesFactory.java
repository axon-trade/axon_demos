package io.xtrd.samples.sts.fix;

import biz.onixs.fix.dictionary.Version;
import biz.onixs.fix.parser.Group;
import biz.onixs.fix.parser.Message;
import biz.onixs.fix.tag.FIX44;
import biz.onixs.fix.tag.Tag;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.trade.Order;
import io.xtrd.samples.sts.fix.commads.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class MessagesFactory {
    private Version version;
    private final DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYYMMdd-HH:mm:ss.SSS");

    public MessagesFactory(Version version) {
        this.version = version;
    }

    public Message getMessage(Command cmd) {
        if(cmd instanceof SecuritiesListRequest) {
            return getSecuritiesListRequest((SecuritiesListRequest) cmd);
        } else if (cmd instanceof MarketDataRequest) {
            return getMarketDataRequest((MarketDataRequest) cmd);
        } else if (cmd instanceof TradingSessionStatusRequest) {
            return getTradingSessionStatusRequest((TradingSessionStatusRequest) cmd);
        } else if (cmd instanceof PositionsRequest) {
            return getPositionsRequest((PositionsRequest) cmd);
        } else if (cmd instanceof NewOrderSingle) {
            return getNewOrderSingle((NewOrderSingle) cmd);
        } else if (cmd instanceof OrderCancelRequest) {
            return getOrderCancelRequest((OrderCancelRequest) cmd);
        } else if (cmd instanceof OrderCancelReplaceRequest) {
            return getOrderCancelReplaceRequest((OrderCancelReplaceRequest) cmd);
        } else {
            throw new RuntimeException("Unsupported command");
        }
    }

    private Message getNewOrderSingle(NewOrderSingle cmd) {
        Message result = Message.create(FIX44.MsgType.NewOrderSingle, version);
        Order order = cmd.getOrder();

        result.set(Tag.Account, order.getLoginId());
        result.set(Tag.ClOrdID, order.getId());
        result.set(Tag.OrderQty, order.getSize());
        switch (order.getType()) {
            case MARKET:
                result.set(Tag.OrdType, FIX44.OrdType.Market);
                break;
            case LIMIT:
                result.set(Tag.OrdType, FIX44.OrdType.Limit);
                result.set(Tag.Price, order.getPrice());
                break;
        }

        populateSize(order.getSide(), result);

        result.set(Tag.Symbol, order.getSymbol().getName());
        result.set(Tag.ExDestination, order.getExchange().getName());

        switch (order.getTimeInForce()) {
            case GOOD_TILL_CANCEL:
                result.set(Tag.TimeInForce, FIX44.TimeInForce.GoodTillCancel);
                break;
            case FILL_OR_KILL:
                result.set(Tag.TimeInForce, FIX44.TimeInForce.FillOrKill);
                break;
            case IMMEDIATE_OR_CANCEL:
                result.set(Tag.TimeInForce, FIX44.TimeInForce.ImmediateOrCancel);
                break;
        }

        result.set(Tag.SecurityType, "CRYPTOSPOT");
        result.set(Tag.TransactTime, fmt.print(order.getCreateTime()));

        return result;
    }

    private Message getOrderCancelRequest(OrderCancelRequest cmd) {
        Message result = Message.create(FIX44.MsgType.OrderCancelRequest, version);
        Order order = cmd.getOrder();

        result.set(Tag.Account, order.getLoginId());
        result.set(Tag.ClOrdID, order.getId());
        result.set(Tag.OrigClOrdID, order.getOriginalId());
        result.set(Tag.OrderID, order.getOrderId());
        result.set(Tag.Symbol, order.getSymbol().getName());
        populateSize(order.getSide(), result);
        result.set(Tag.TransactTime, fmt.print(getTimestamp()));

        return result;
    }

    private Message getOrderCancelReplaceRequest(OrderCancelReplaceRequest cmd) {
        Message result = Message.create(FIX44.MsgType.OrderCancelReplaceRequest, version);
        Order order = cmd.getOrder();

        result.set(Tag.Account, order.getLoginId());
        result.set(Tag.ClOrdID, order.getId());
        result.set(Tag.OrigClOrdID, order.getOriginalId());
        result.set(Tag.OrderID, order.getOrderId());
        result.set(Tag.Symbol, order.getSymbol().getName());
        populateSize(order.getSide(), result);
        result.set(Tag.TransactTime, fmt.print(getTimestamp()));
        result.set(Tag.Price, order.getPrice());
        result.set(Tag.OrderQty, order.getSize());

        return result;
    }


    private void populateSize(Order.Side side, Message msg) {
        switch (side) {
            case BUY:
                msg.set(Tag.Side, FIX44.Side.Buy);
                break;
            case SELL:
                msg.set(Tag.Side, FIX44.Side.Sell);
                break;
        }
    }

    private Message getPositionsRequest(PositionsRequest cmd) {
        DateTime timestamp = getTimestamp();
        Message result = Message.create(FIX44.MsgType.RequestForPositions, version);
        result.set(Tag.PosReqID, cmd.getRequestId());
        result.set(Tag.PosReqType, FIX44.PosReqType.Positions);
        result.set(Tag.SubscriptionRequestType, FIX44.SubscriptionRequestType.SnapshotUpdate);
        result.set(Tag.Account, cmd.getOrganization().getId());
        result.set(Tag.AccountType, FIX44.AccountType.AccountCustomer);
        result.set(Tag.ClearingBusinessDate, timestamp.toString());
        result.set(Tag.TransactTime, timestamp.toString());

        Group group = result.setGroup(Tag.NoPartyIDs, cmd.getLogins().size());
        for(int i = 0;i < cmd.getLogins().size();i++) {
            group.set(Tag.PartyID, i, cmd.getLogins().get(i).getId());
            group.set(Tag.PartyRole, i, FIX44.PartyRole.InitiatingTrader);
        }

        return result;
    }

    private Message getTradingSessionStatusRequest(TradingSessionStatusRequest cmd) {
        Message result = Message.create(FIX44.MsgType.TradingSessionStatusRequest, version);
        result.set(Tag.TradSesReqID, cmd.getRequestId());
        result.set(Tag.SubscriptionRequestType, FIX44.SubscriptionRequestType.SnapshotUpdate);

        return result;
    }

    private Message getMarketDataRequest(MarketDataRequest cmd) {
        Message result = Message.create(FIX44.MsgType.MarketDataRequest, version);
        result.set(Tag.MDReqID, cmd.getRequestId());
        if(cmd.getType() == MarketDataRequest.Type.SUBSCRIBE) {
            result.set(Tag.SubscriptionRequestType, FIX44.SubscriptionRequestType.SnapshotUpdate);
            result.set(Tag.MarketDepth, 0);
            result.set(Tag.MDUpdateType, FIX44.MDUpdateType.Incremental);
            Group entries = result.setGroup(Tag.NoMDEntryTypes, 3);
            entries.set(Tag.MDEntryType, 0, FIX44.MDEntryType.Bid);
            entries.set(Tag.MDEntryType, 1, FIX44.MDEntryType.Offer);
            entries.set(Tag.MDEntryType, 2, FIX44.MDEntryType.Trade);

            Group symbols = result.setGroup(Tag.NoRelatedSym, cmd.getSymbols().size());
            int idx = 0;
            for(Symbol symbol : cmd.getSymbols()) {
                symbols.set(Tag.Symbol, idx, symbol.getName());
                symbols.set(Tag.SecurityExchange, idx, symbol.getExchange().getName());
                idx++;
            }
        } else {
            result.set(Tag.SubscriptionRequestType, FIX44.SubscriptionRequestType.Unsubscribe);
            Group symbols = result.setGroup(Tag.NoRelatedSym, cmd.getSymbols().size());
            int idx = 0;
            for(Symbol symbol : cmd.getSymbols()) {
                symbols.set(Tag.Symbol, idx, symbol.getName());
                symbols.set(Tag.SecurityExchange, idx, symbol.getExchange().getName());
                idx++;
            }
        }

        return result;
    }

    private Message getSecuritiesListRequest(SecuritiesListRequest cmd) {
        Message result = Message.create(FIX44.MsgType.SecurityListRequest, version);
        result.set(Tag.SecurityReqID, cmd.getRequestId());
        result.set(Tag.SecurityListRequestType, FIX44.SecurityListRequestType.AllSecurities);
        result.set(Tag.SecurityExchange, cmd.getExchange().getName());

        return result;
    }

    private DateTime getTimestamp() {
        return new DateTime().withZone(DateTimeZone.UTC);
    }
}
