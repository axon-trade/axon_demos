package io.xtrd.samples.sts.fix.commads;

import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.md.Exchange;

public class SecuritiesListRequest extends Command {
    private Exchange exchange;

    public SecuritiesListRequest(long requestId, Exchange exchange, int sessionId) {
        super(requestId, sessionId);
        this.exchange = exchange;
    }

    public Exchange getExchange() {
        return exchange;
    }
}
