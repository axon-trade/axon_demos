package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.Command;

import java.util.function.BiConsumer;

public class AddAccount extends Command {
    private Account account;
    private BiConsumer<Account, Login>consumer;

    public AddAccount(long requestId, Account account, BiConsumer<Account, Login>consumer) {
        super(requestId, -1);
        this.account = account;
        this.consumer = consumer;
    }

    public Account getAccount() {
        return account;
    }

    public BiConsumer<Account, Login> getConsumer() {
        return consumer;
    }
}
