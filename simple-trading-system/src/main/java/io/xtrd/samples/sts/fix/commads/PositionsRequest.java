package io.xtrd.samples.sts.fix.commads;

import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.org.Organization;

import java.util.List;

public class PositionsRequest extends Command {
    private Organization organization;
    private List<Login> logins;

    public PositionsRequest(long requestId, int sessionId, Organization organization, List<Login>logins) {
        super(requestId, sessionId);
        this.organization = organization;
        this.logins = logins;
    }

    public List<Login> getLogins() {
        return logins;
    }

    public Organization getOrganization() {
        return organization;
    }
}
