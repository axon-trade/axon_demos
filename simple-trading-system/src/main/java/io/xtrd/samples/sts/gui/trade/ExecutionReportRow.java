package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.common.trade.ExecutionReport;
import io.xtrd.samples.common.trade.Order;
import javafx.beans.property.*;
import org.joda.time.DateTime;

public class ExecutionReportRow {
    private SimpleIntegerProperty id;
    private SimpleLongProperty orderId;
    private SimpleStringProperty symbol;
    private SimpleStringProperty exchange;
    private SimpleObjectProperty<Order.Status>status;
    private SimpleDoubleProperty lastPrice;
    private SimpleDoubleProperty lastSize;
    private SimpleDoubleProperty commission;
    private SimpleStringProperty commissionAsset;
    private SimpleObjectProperty<DateTime>transactionTime;
    private SimpleStringProperty text;

    public ExecutionReportRow(ExecutionReport report) {
        id = new SimpleIntegerProperty(report.getId());
        orderId = new SimpleLongProperty(report.getOrderId());
        symbol = new SimpleStringProperty(report.getSymbol().getName());
        exchange = new SimpleStringProperty(report.getExchange().getName());
        status = new SimpleObjectProperty<>(report.getStatus());
        lastPrice = new SimpleDoubleProperty(report.getLastPrice());
        lastSize = new SimpleDoubleProperty(report.getLastSize());
        commission = new SimpleDoubleProperty(report.getCommissionSize());
        if(report.getCommissionAsset() != null) {
            commissionAsset = new SimpleStringProperty(report.getCommissionAsset().getName());
        } else {
            commissionAsset = new SimpleStringProperty("");
        }
        transactionTime = new SimpleObjectProperty<>(report.getTransactionTime());
        text = new SimpleStringProperty(report.getText());
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public SimpleLongProperty orderIdProperty() {
        return orderId;
    }

    public SimpleStringProperty symbolProperty() {
        return symbol;
    }

    public SimpleStringProperty exchangeProperty() {
        return exchange;
    }

    public SimpleObjectProperty<Order.Status>statusProperty() {
        return status;
    }

    public SimpleDoubleProperty lastPriceProperty() {
        return lastPrice;
    }

    public SimpleDoubleProperty lastSizeProperty() {
        return lastSize;
    }

    public SimpleDoubleProperty commissionProperty() {
        return commission;
    }

    public SimpleStringProperty commissionAssetProperty() {
        return commissionAsset;
    }

    public SimpleObjectProperty<DateTime>transactionProperty() {
        return transactionTime;
    }

    public SimpleStringProperty textProperty() {
        return text;
    }
}
