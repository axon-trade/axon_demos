package io.xtrd.samples.sts;

public class Constants {
    public interface Api {
        String ID = "id";
        String NAME = "name";
        String FULL_NAME = "full_name";
        String DESCRIPTION = "description";
        String STATUS = "status";
        String ORGANIZATION_ID = "organization_id";
        String LOGIN = "login";
        String ROUTER_ID = "router_id";
        String ROUTE_ID = "route_id";
        String KEY = "key";
        String VALUE = "value";
        String SETTINGS = "settings";
        String PARAMS = "params";
    }

    public interface Headers {
        String API_KEY = "Api-Key-Token";
    }

    public interface Settings {
        String FIX_ENGINE_CONFIG = "fix.engine_config";
        String DIALECT_NAME = "fix.dialect";
        String SESSION_BASE = "sessions";
        String HOST = "host";
        String PORT = "port";
        String TARGET_COMP_ID = "target_comp_id";
        String SENDER_COMP_ID = "sender_comp_id";
        String USERNAME = "username";
        String PASSWORD = "password";
        String HEARTBEAT = "heartbeat";
        String USE_SSL = "use_ssl";
        String SESSION_TYPE = "session_type";

        String BASE_API_URL = "management.base_api_url";
        String API_TOKEN = "management.api_token";
        String ORGANIZATION_ID = "organization_id";

        String SYNC_TIMEOUT = "management.sync_timeout";
    }

    public interface SessionTypes {
        String MARKET_DATA = "market_data";
        String TRADING = "trading";
    }
}
