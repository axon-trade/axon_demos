package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.trade.Order;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.gui.utils.FieldsHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class OrderController implements Initializable {
    @Inject private Core core;

    @FXML ChoiceBox<Account>accountSelector;
    @FXML ChoiceBox<Exchange>exchangeSelector;
    @FXML ChoiceBox<Symbol>symbolSelector;
    @FXML RadioButton sideBuyRadioButton;
    @FXML RadioButton sideSellRadioButton;
    @FXML TextField sizeField;
    @FXML ChoiceBox<Order.Type>typeSelector;
    @FXML TextField priceField;
    @FXML ChoiceBox<Order.TimeInForce>tifSelector;
    @FXML Button sendButton;
    @FXML Button cancelButton;

    private ButtonType action;

    public ButtonType getButtonType() {
        return action;
    }

    public Order getOrder() {
        Login login = core.getOrganizationsModel().getLogins(accountSelector.getValue().getId()).get(0);
        Order result = new Order(Order.ExecType.TRADE);

        result.setLoginId(login.getId());
        result.setExchange(exchangeSelector.getValue());
        result.setSymbol(symbolSelector.getValue());
        if(sideBuyRadioButton.isSelected()) {
            result.setSide(Order.Side.BUY);
        } else {
            result.setSide(Order.Side.SELL);
        }
        result.setSize(Double.parseDouble(sizeField.getText()));
        result.setType(typeSelector.getValue());
        if(result.getType() == Order.Type.LIMIT) {
            result.setPrice(Double.parseDouble(priceField.getText()));
        }
        result.setCreateTime(new DateTime());
        result.setStatus(Order.Status.UNDEFINED);
        result.setTimeInForce(tifSelector.getValue());

        return result;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        accountSelector.setConverter(FieldsHelper.getAccountStringConverter());
        accountSelector.getItems().addAll(core.getOrganizationsModel().getAccounts());

        exchangeSelector.setConverter(FieldsHelper.getStringConverter());
        exchangeSelector.getItems().addAll(core.getMarketDataModel().getExchanges());

        exchangeSelector.getSelectionModel().selectedItemProperty().addListener((obs, old, newValue) -> {
            symbolSelector.getItems().clear();
            symbolSelector.getItems().addAll(core.getMarketDataModel().getSymbols(newValue));
        });

        symbolSelector.setConverter(FieldsHelper.getSymbolStringConverter());

        typeSelector.getItems().addAll(Order.Type.MARKET, Order.Type.LIMIT);
        tifSelector.getItems().addAll(Order.TimeInForce.GOOD_TILL_CANCEL, Order.TimeInForce.FILL_OR_KILL, Order.TimeInForce.IMMEDIATE_OR_CANCEL);
        tifSelector.setValue(Order.TimeInForce.GOOD_TILL_CANCEL);

        sendButton.setOnAction(e -> {
            action = ButtonType.OK;
            closeWindow((Node) e.getSource());
        });

        cancelButton.setOnAction(e -> {
            action = ButtonType.CANCEL;
            closeWindow((Node) e.getSource());
        });
    }

    private void closeWindow(Node source) {
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
