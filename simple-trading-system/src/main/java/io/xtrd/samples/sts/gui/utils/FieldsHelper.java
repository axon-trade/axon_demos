package io.xtrd.samples.sts.gui.utils;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.common.org.Account;
import javafx.util.StringConverter;

public class FieldsHelper {
    public static StringConverter<Exchange> getStringConverter() {
        return new StringConverter<Exchange>() {
            @Override
            public String toString(Exchange object) {
                if(object != null) {
                    return object.getFullName();
                } else {
                    return "";
                }
            }

            @Override
            public Exchange fromString(String exchangeName) {
                return null;
            }
        };
    }

    public static StringConverter<Account> getAccountStringConverter() {
        return new StringConverter<Account>() {
            @Override
            public String toString(Account object) {
                if(object != null) {
                    return object.getName();
                } else {
                    return "";
                }
            }

            @Override
            public Account fromString(String exchangeName) {
                return null;
            }
        };
    }

    public static StringConverter<Symbol> getSymbolStringConverter() {
        return new StringConverter<Symbol>() {
            @Override
            public String toString(Symbol object) {
                if(object != null) {
                    return object.getName();
                } else {
                    return "";
                }
            }

            @Override
            public Symbol fromString(String exchangeName) {
                return null;
            }
        };
    }
}
