package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.trade.ExecutionReport;
import io.xtrd.samples.common.trade.Order;
import javafx.beans.property.*;
import org.joda.time.DateTime;

public class OrderRow {
    private Order order;
    private SimpleIntegerProperty id;
    private SimpleLongProperty orderId;
    private SimpleStringProperty secondaryOrderId;
    private SimpleStringProperty account;
    private SimpleStringProperty symbol;
    private SimpleStringProperty exchange;
    private SimpleObjectProperty<Order.Type>type;
    private SimpleObjectProperty<Order.Side>side;
    private SimpleObjectProperty<DateTime>createTime;
    private SimpleObjectProperty<DateTime>lastUpdateTime;
    private SimpleDoubleProperty size;
    private SimpleDoubleProperty price;
    private SimpleDoubleProperty filledSize;
    private SimpleObjectProperty<Order.Status>status;

    public OrderRow(Account account, Order order) {
        this.order = order;
        id = new SimpleIntegerProperty(order.getId());
        orderId = new SimpleLongProperty(order.getOrderId());
        secondaryOrderId = new SimpleStringProperty(order.getSecondaryOrderId());
        this.account = new SimpleStringProperty(account.getName());
        symbol = new SimpleStringProperty(order.getSymbol().getName());
        exchange = new SimpleStringProperty(order.getExchange().getName());
        type = new SimpleObjectProperty<>(order.getType());
        side = new SimpleObjectProperty<>(order.getSide());
        createTime = new SimpleObjectProperty<>(order.getCreateTime());
        lastUpdateTime = new SimpleObjectProperty<>(order.getLastUpdateTime());
        size = new SimpleDoubleProperty(order.getSize());
        price = new SimpleDoubleProperty(order.getPrice());
        filledSize = new SimpleDoubleProperty(order.getFilledSize());
        status = new SimpleObjectProperty<>(order.getStatus());
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public SimpleLongProperty orderIdProperty() {
        return orderId;
    }

    public SimpleStringProperty secondaryOrderIdProperty() {
        return secondaryOrderId;
    }

    public SimpleStringProperty accountProperty() {
        return account;
    }

    public SimpleStringProperty symbolProperty() {
        return symbol;
    }

    public SimpleStringProperty exchangeProperty() {
        return exchange;
    }

    public SimpleObjectProperty<Order.Type>typeProperty() {
        return type;
    }

    public SimpleObjectProperty<Order.Side>sideProperty() {
        return side;
    }

    public SimpleObjectProperty<DateTime>createTimeProperty() {
        return createTime;
    }

    public SimpleObjectProperty<DateTime>lastUpdateTimeProperty() {
        return lastUpdateTime;
    }

    public SimpleDoubleProperty sizeProperty() {
        return size;
    }

    public SimpleDoubleProperty priceProperty() {
        return price;
    }

    public SimpleDoubleProperty filledSizeProperty() {
        return filledSize;
    }

    public SimpleObjectProperty<Order.Status>statusProperty() {
        return status;
    }

    public boolean update(ExecutionReport report) {
        boolean finished = false;
        switch (report.getStatus()) {
            case PENDING_NEW:
                orderId.set(report.getOrderId());
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());
                break;
            case NEW:
                order.setOrderId(report.getOrderId());
                secondaryOrderId.set(report.getSecondaryOrderId());
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());
            case PARTIALLY_FILLED:
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());
                filledSize.set(report.getFilledSize());
            case FILLED:
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());
                filledSize.set(report.getFilledSize());
                break;
            case CANCELED:
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());

                finished = true;
                break;
            case REJECTED:
                status.set(report.getStatus());
                lastUpdateTime.set(report.getTransactionTime());
                finished = true;
                break;
        }

        return finished;
    }

    public Order getOrder() {
        return order;
    }
}
