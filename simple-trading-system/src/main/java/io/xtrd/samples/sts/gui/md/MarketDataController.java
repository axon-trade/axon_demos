package io.xtrd.samples.sts.gui.md;

import io.xtrd.samples.common.md.Book;
import io.xtrd.samples.common.md.MarketDataEvent;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.fix.commads.MarketDataRequest;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.LinkedBlockingQueue;

public class MarketDataController implements Initializable {
    @Inject private Core core;
    @FXML TableView<BookRow>booksTable;
    @FXML Button symbolsBrowserButton;
    private LinkedBlockingQueue<MarketDataEvent[]>queue = new LinkedBlockingQueue<>();
    private Map<Integer, BookRow> booksMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initBooksTable();
        symbolsBrowserButton.setOnAction(this::onBrowseSymbols);

        core.addMarketDataListener(events -> queue.add(events));

        Thread worker = new Thread(new Worker());
        worker.start();
    }

    private void initBooksTable() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem unsubscribeItem = new MenuItem("Unsubscribe");
        unsubscribeItem.setOnAction(this::onUnsubscribe);
        contextMenu.getItems().add(unsubscribeItem);

        booksTable.setRowFactory(param -> {
            TableRow<BookRow> row = new TableRow<>();
            row.contextMenuProperty().bind(Bindings.when(Bindings.isNotNull(row.itemProperty())).then(contextMenu).otherwise((ContextMenu) null));

            return row;
        });
    }

    private void onBrowseSymbols(ActionEvent event) {
        BrowserView dialog = new BrowserView();
        Scene scene = new Scene(dialog.getView());
        Stage stage = new Stage();
        stage.initModality(Modality.NONE);
        stage.setScene(scene);
        stage.setTitle("Symbols Browser");

        BrowserController controller = (BrowserController) dialog.getPresenter();
        controller.setMarketDataProvider(symbol -> {
            BookRow bookRow = new BookRow(new Book(symbol));
            booksMap.put(symbol.getId(), bookRow);
            booksTable.getItems().add(bookRow);

            core.getSessionsManager().send(new MarketDataRequest(
                core.getNextId(),
                1,
                MarketDataRequest.Type.SUBSCRIBE,
                Arrays.asList(symbol)
            ));
        });

        stage.show();
    }

    private void onUnsubscribe(ActionEvent event) {
        BookRow bookRow = booksTable.getSelectionModel().getSelectedItem();

        core.getSessionsManager().send(new MarketDataRequest(
            core.getNextId(),
            1,
            MarketDataRequest.Type.UNSUBSCRIBE,
            Arrays.asList(bookRow.getSymbol())
        ));

        booksTable.getItems().remove(bookRow);
        booksMap.remove(bookRow.getSymbol().getId());
    }

    private void processEvents(MarketDataEvent events[]) {
        BookRow book = booksMap.get(events[0].getSymbolId());
        if(book != null) book.processEvents(events);
    }

    private class Worker implements Runnable {
        @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()) {
                try {
                    MarketDataEvent events[] = queue.take();
                    processEvents(events);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {

                }
            }
        }
    }
}
