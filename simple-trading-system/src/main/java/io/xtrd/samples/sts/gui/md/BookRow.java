package io.xtrd.samples.sts.gui.md;

import io.xtrd.samples.common.md.Book;
import io.xtrd.samples.common.md.MarketDataEvent;
import io.xtrd.samples.common.md.Symbol;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.joda.time.DateTime;

public class BookRow {
    private final Book book;
    private final Symbol symbol;

    private SimpleIntegerProperty id;
    private SimpleStringProperty name;
    private SimpleStringProperty exchangeName;
    private SimpleDoubleProperty bestBid;
    private SimpleDoubleProperty bestBidSize;
    private SimpleDoubleProperty bestAsk;
    private SimpleDoubleProperty bestAskSize;
    private SimpleObjectProperty<DateTime> lastUpdateTime;
    private SimpleObjectProperty<Symbol> symbolProperty;

    public BookRow(Book book) {
        this.book = book;
        this.symbol = book.getSymbol();
        id = new SimpleIntegerProperty(symbol.getId());
        name = new SimpleStringProperty(symbol.getName());
        exchangeName = new SimpleStringProperty(symbol.getExchange().getName());
        bestBid = new SimpleDoubleProperty(0);
        bestBidSize = new SimpleDoubleProperty(0);
        bestAsk = new SimpleDoubleProperty(0);
        bestAskSize = new SimpleDoubleProperty(0);
        lastUpdateTime = new SimpleObjectProperty<>();
        symbolProperty = new SimpleObjectProperty<>(symbol);
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public SimpleObjectProperty<Symbol> symbolProperty() {
        return symbolProperty;
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleStringProperty exchangeProperty() {
        return exchangeName;
    }

    public SimpleDoubleProperty bestBidProperty() {
        return bestBid;
    }

    public SimpleDoubleProperty bestBidSizeProperty() {
        return bestBidSize;
    }

    public SimpleDoubleProperty bestAskProperty() {
        return bestAsk;
    }

    public SimpleDoubleProperty bestAskSizeProperty() {
        return bestAskSize;
    }

    public SimpleObjectProperty<DateTime> lastUpdateTimeProperty() {
        return lastUpdateTime;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void processEvents(MarketDataEvent events[]) {
        try {
            book.processEvents(events);
            MarketDataEvent snapshot = book.getTopOfTheBook(MarketDataEvent.Side.BID);
            if (snapshot != null) {
                bestBid.set(Symbol.castToDouble(snapshot.getPrice(), symbol.getPriceFactor()));
                bestBidSize.set(Symbol.castToDouble(snapshot.getSize(), symbol.getSizeFactor()));
                lastUpdateTime.set(new DateTime(snapshot.getTimestamp()));
            }

            snapshot = book.getTopOfTheBook(MarketDataEvent.Side.ASK);
            if (snapshot != null) {
                bestAsk.set(Symbol.castToDouble(snapshot.getPrice(), symbol.getPriceFactor()));
                bestAskSize.set(Symbol.castToDouble(snapshot.getSize(), symbol.getSizeFactor()));
                lastUpdateTime.set(new DateTime(snapshot.getTimestamp()));
            }
        } catch (Exception e) {
            System.err.println("Error on " + symbol.getName() + ": " + e.getMessage());
        }
    }
}
