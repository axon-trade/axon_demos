package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.Command;

import java.util.List;
import java.util.function.Consumer;

public class GetLogins extends Command {
    private Account account;
    private Consumer<List<Login>>consumer;

    public GetLogins(long requestId, Account account, Consumer<List<Login>> consumer) {
        super(requestId, -1);
        this.account = account;
        this.consumer = consumer;
    }

    public Account getAccount() {
        return account;
    }

    public Consumer<List<Login>> getConsumer() {
        return consumer;
    }
}
