package io.xtrd.samples.sts.org;

import io.xtrd.samples.common.Connector;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.org.Organization;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.sts.rest.ApiClient;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class OrganizationsModel {
    private final Logger logger = LoggerFactory.getLogger(OrganizationsModel.class);
    private ApiClient apiClient;
    private AtomicLong idGenerator = new AtomicLong(0);
    private Organization organization = null;
    private String routerId = "1";
    private Map<Integer, Account> accountsMap = new HashMap<>();
    private Map<Integer, Login>loginsMap = new HashMap<>();
    private Map<Integer, List<Login>>accountLogins = new HashMap<>();
    private Map<Integer, List<Connector>>accountConnectors = new HashMap<>();
    private Properties config;
    private long syncTimeout;

    public void addAccount(Account account, Consumer<Account> consumer) {
        apiClient.addAccount(idGenerator.incrementAndGet(), account, (newAccount, login) -> {
            accountsMap.put(account.getId(), account);
            ArrayList<Login>logins = new ArrayList<>();
            logins.add(login);
            accountLogins.put(account.getId(), logins);

            apiClient.changeAccountProperties(
                    idGenerator.incrementAndGet(),
                    newAccount,
                    Arrays.asList(new Pair<>(Constants.Api.ROUTER_ID, routerId)),
                    ack -> {
                        consumer.accept(newAccount);
            });
        });
    }

    public void addConnector(Account account, Exchange exchange, List<Pair<String, String>>settings, Consumer<Connector>consumer) {
        apiClient.addConnector(idGenerator.incrementAndGet(), account, exchange, settings, connector -> {
            accountConnectors.get(account.getId()).add(connector);
            consumer.accept(connector);
        });
    }

    public Organization getOrganization() {
        return organization;
    }

    public void deleteAccount(Account account, Consumer<Boolean>consumer) {
        apiClient.deleteAccount(idGenerator.incrementAndGet(), account, ack -> {
            accountConnectors.remove(account.getId());
            List<Login>logins = accountLogins.remove(account.getId());
            if(logins != null) logins.forEach(login -> loginsMap.remove(login.getId()));
            accountsMap.remove(account.getId());

            consumer.accept(Boolean.TRUE);
        });
    }

    public Account getAccount(int accountId) {
        return accountsMap.get(accountId);
    }

    public List<Account>getAccounts() {
        ArrayList<Account> result = new ArrayList<>(accountsMap.values());
        result.sort(Comparator.comparing(Account::getName));

        return result;
    }

    public List<Login>getLogins(int accountId) {
        return accountLogins.get(accountId);
    }

    public Login getLogin(int loginId) {
        return loginsMap.get(loginId);
    }


    public List<Connector>getConnectors(int accountId) {
        return accountConnectors.get(accountId);
    }

    public void init() throws Exception {
        syncTimeout = Long.parseLong(config.getProperty(Constants.Settings.SYNC_TIMEOUT, "5000"));

        int organizationId = Integer.parseInt(config.getProperty(Constants.Settings.ORGANIZATION_ID));
        AtomicReference<Organization>orgReference = new AtomicReference<>();
        CountDownLatch orgLatch = new CountDownLatch(1);
        apiClient.getOrganization(idGenerator.incrementAndGet(), organizationId, org -> {
            orgReference.set(org);
            orgLatch.countDown();
        });
        if(!orgLatch.await(syncTimeout, TimeUnit.MILLISECONDS)) throw new Exception("Timeout during fetching Organization");
        organization = new Organization(organizationId, orgReference.get().getName());

        if(logger.isDebugEnabled()) logger.debug("Starting Accounts synchronization");
        CountDownLatch accountsLatch = new CountDownLatch(1);
        apiClient.getAccounts(idGenerator.incrementAndGet(), accounts -> {
            if(logger.isDebugEnabled()) logger.debug("Received {} accounts", accounts.size());

            for(Account account : accounts) {
                account.setOrganizationId(organization.getId());
                accountsMap.put(account.getId(), account);
            }
            accountsLatch.countDown();
        });
        if(accountsLatch.await(syncTimeout, TimeUnit.MILLISECONDS)) {
            if(logger.isDebugEnabled()) logger.debug("Accounts synchronization is finished, extacted {} accounts", accountsMap.size());
        } else {
            logger.error("Failed to synchronize accounts");
        }

        CountDownLatch accountSyncLatch = new CountDownLatch(accountsMap.size() * 2);//we do two extra calls from here - logins and connectors
        for(Account account : accountsMap.values()) {
            //Get connectors
            apiClient.getLogins(idGenerator.incrementAndGet(), account, logins -> {
                for(Login login : logins) {
                    loginsMap.put(login.getId(), login);
                    accountLogins.put(account.getId(), logins);
                }
                if(logger.isDebugEnabled()) logger.debug("Logins synchronization for Account '{}' (ID: {}) finished. Extracted {} logins", account.getName(), account.getId(), logins.size());
                accountSyncLatch.countDown();
            });
            apiClient.getConnectors(idGenerator.incrementAndGet(), account, connectors -> {
                accountConnectors.put(account.getId(), connectors);
                if(logger.isDebugEnabled()) logger.debug("Connectors synchronization for Account '{}' (ID: {}) finished. Extracted {} connectors", account.getName(), account.getId(), connectors.size());
                accountSyncLatch.countDown();
            });
        }
        if(accountSyncLatch.await(syncTimeout, TimeUnit.MILLISECONDS)) {
            if(logger.isDebugEnabled()) logger.debug("Accounts assets synchronization is finished");
        } else {
            logger.error("Failed to synchronize accounts assets");
        }

    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }
}
