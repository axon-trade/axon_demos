package io.xtrd.samples.sts.gui.sessions;

import io.xtrd.samples.sts.Core;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

import javax.inject.Inject;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class SessionsController implements Initializable {
    @Inject private Core core;
    @FXML TableView<SessionRow> sessionsTable;
    private Map<Integer, SessionRow> sessionsMap = new HashMap<>();
    private ContextMenu contextMenu;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initContextMenu();
        sessionsTable.setRowFactory(param -> {
            TableRow<SessionRow> row = new TableRow<>();
            row.contextMenuProperty().bind(Bindings.when(Bindings.isNotNull(row.itemProperty())).then(contextMenu).otherwise((ContextMenu) null));

            return row;
        });

        core.getSessionsManager().getCredentials().forEach(credentials -> {
            SessionRow sessionRow = new SessionRow(credentials);
            sessionsMap.put(credentials.getId(), sessionRow);
            sessionsTable.getItems().add(sessionRow);
        });

        core.addConnectionStatusListener(event -> sessionsMap.get(event.getSessionId()).updateStatus(event.getStatus()));
    }

    private void initContextMenu() {
        contextMenu = new ContextMenu();
        MenuItem startItem = new MenuItem("Start");
        startItem.setOnAction(this::onStart);

        MenuItem stopItem = new MenuItem("Stop");
        stopItem.setOnAction(this::onStop);

        contextMenu.getItems().addAll(startItem, stopItem);
    }

    private void onStart(ActionEvent e) {
        core.getSessionsManager().start(sessionsTable.getSelectionModel().getSelectedItem().idProperty().get());
    }

    private void onStop(ActionEvent e) {
        core.getSessionsManager().stop(sessionsTable.getSelectionModel().getSelectedItem().idProperty().get());
    }


}
