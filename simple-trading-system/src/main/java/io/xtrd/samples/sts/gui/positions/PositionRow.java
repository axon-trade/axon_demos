package io.xtrd.samples.sts.gui.positions;

import io.xtrd.samples.common.trade.Position;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class PositionRow {
    protected SimpleStringProperty asset;
    protected SimpleStringProperty exchange;
    protected SimpleDoubleProperty size;

    public PositionRow(Position position) {
        this.asset = new SimpleStringProperty(position.getAsset().getName());
        this.exchange = new SimpleStringProperty(position.getExchange().getName());
        this.size = new SimpleDoubleProperty(position.getSize());
    }

    public SimpleStringProperty assetProperty() {
        return asset;
    }

    public SimpleStringProperty exchangeProperty() {
        return exchange;
    }

    public SimpleDoubleProperty sizeProperty() {
        return size;
    }

    public void update(Position position) {
        this.size.set(position.getSize());
    }
}
