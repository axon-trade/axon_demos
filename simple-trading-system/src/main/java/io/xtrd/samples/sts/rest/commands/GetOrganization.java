package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.org.Organization;

import java.util.function.Consumer;

public class GetOrganization extends Command {
    private Consumer<Organization>consumer;

    public GetOrganization(long requestId, Consumer<Organization> consumer) {
        super(requestId, -1);
        this.consumer = consumer;
    }

    public Consumer<Organization> getConsumer() {
        return consumer;
    }
}
