package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.Connector;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.Command;
import javafx.util.Pair;

import java.util.List;
import java.util.function.Consumer;

public class AddConnector extends Command {
    private Account account;
    private Exchange exchange;
    private List<Pair<String, String>>settings;
    private Consumer<Connector> consumer;

    public AddConnector(long requestId, Account account, Exchange exchange, List<Pair<String, String>> settings, Consumer<Connector> consumer) {
        super(requestId, -1);
        this.account = account;
        this.exchange = exchange;
        this.settings = settings;
        this.consumer = consumer;
    }

    public Account getAccount() {
        return account;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public Consumer<Connector> getConsumer() {
        return consumer;
    }

    public List<Pair<String, String>> getSettings() {
        return settings;
    }
}
