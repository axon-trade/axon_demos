package io.xtrd.samples.sts.gui.management;

import javafx.beans.property.SimpleStringProperty;
import javafx.util.Pair;

public class SettingRow {
    private final SimpleStringProperty key;
    private final SimpleStringProperty value;
    private Pair<String, String>pair;

    public SettingRow(Pair<String, String> pair) {
        this.pair = pair;
        this.key = new SimpleStringProperty(pair.getKey());
        this.value = new SimpleStringProperty(pair.getValue());
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public SimpleStringProperty valueProperty() {
        return value;
    }

    public void clear() {
        value.set("");
    }

    public Pair<String, String>getPair() {
        return new Pair<>(key.get(), value.get());
    }
}