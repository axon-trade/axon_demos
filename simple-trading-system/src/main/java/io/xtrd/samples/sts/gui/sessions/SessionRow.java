package io.xtrd.samples.sts.gui.sessions;

import io.xtrd.samples.common.fix.ConnectionStatusEvent;
import io.xtrd.samples.common.fix.Credentials;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class SessionRow {
    private SimpleIntegerProperty id;
    private SimpleStringProperty host;
    private SimpleIntegerProperty port;
    private SimpleStringProperty targetCompId;
    private SimpleStringProperty senderCompId;
    private SimpleStringProperty username;
    private SimpleObjectProperty<Credentials.SessionType>sessionType;
    private SimpleObjectProperty<ConnectionStatusEvent.Status> status;

    public SessionRow(Credentials credentials) {
        id = new SimpleIntegerProperty(credentials.getId());
        host = new SimpleStringProperty(credentials.getHost());
        port = new SimpleIntegerProperty(credentials.getPort());
        targetCompId = new SimpleStringProperty(credentials.getTargetCompId());
        senderCompId = new SimpleStringProperty(credentials.getSenderCompId());
        username = new SimpleStringProperty(credentials.getUsername());
        sessionType = new SimpleObjectProperty<>(credentials.getSessionType());
        status = new SimpleObjectProperty<>(ConnectionStatusEvent.Status.DISCONNECTED);
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public SimpleStringProperty hostProperty() {
        return host;
    }

    public SimpleIntegerProperty portProperty() {
        return port;
    }

    public SimpleStringProperty targetCompIdProperty() {
        return targetCompId;
    }

    public SimpleStringProperty senderCompIdProperty() {
        return senderCompId;
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public SimpleObjectProperty<Credentials.SessionType> sessionTypeProperty() {
        return sessionType;
    }

    public SimpleObjectProperty<ConnectionStatusEvent.Status> statusProperty() {
        return status;
    }

    public void updateStatus(ConnectionStatusEvent.Status status) {
        this.status.set(status);
    }

}
