package io.xtrd.samples.sts.fix.commads;

import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.md.Symbol;

import java.util.List;

public class MarketDataRequest extends Command {
    public enum Type {SUBSCRIBE, UNSUBSCRIBE}

    private Type type;
    private List<Symbol> symbols;

    public MarketDataRequest(long requestId, int sessionId, Type type, List<Symbol>symbols) {
        super(requestId, sessionId);
        this.type = type;
        this.symbols = symbols;
    }

    public Type getType() {
        return type;
    }

    public List<Symbol> getSymbols() {
        return symbols;
    }
}
