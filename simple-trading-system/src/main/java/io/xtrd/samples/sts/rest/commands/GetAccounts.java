package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.Command;

import java.util.List;
import java.util.function.Consumer;

public class GetAccounts extends Command {
    private Consumer<List<Account>> consumer;

    public GetAccounts(long requestId, Consumer<List<Account>>consumer) {
        super(requestId, -1);
        this.consumer = consumer;
    }

    public Consumer<List<Account>> getConsumer() {
        return consumer;
    }
}
