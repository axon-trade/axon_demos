package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.Command;

import java.util.List;
import java.util.function.Consumer;

public class GetRoutes extends Command {
    private Consumer<List<Exchange>>consumer;

    public GetRoutes(long requestId, Consumer<List<Exchange>> consumer) {
        super(requestId, -1);
        this.consumer = consumer;
    }

    public Consumer<List<Exchange>> getConsumer() {
        return consumer;
    }
}
