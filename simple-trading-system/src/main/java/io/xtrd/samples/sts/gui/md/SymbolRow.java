package io.xtrd.samples.sts.gui.md;

import io.xtrd.samples.common.md.Symbol;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SymbolRow {
    private Symbol symbol;
    private SimpleStringProperty name;
    private SimpleIntegerProperty pricePower;
    private SimpleIntegerProperty sizePower;
    private SimpleDoubleProperty minOrderSize;

    public SymbolRow(Symbol symbol) {
        this.symbol = symbol;
        name = new SimpleStringProperty(symbol.getName());
        pricePower = new SimpleIntegerProperty(symbol.getPricePower());
        sizePower = new SimpleIntegerProperty(symbol.getSizePower());
        minOrderSize = new SimpleDoubleProperty(symbol.getMinOrderSize());
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleIntegerProperty pricePowerProperty() {
        return pricePower;
    }

    public SimpleIntegerProperty sizePowerProperty() {
        return sizePower;
    }

    public SimpleDoubleProperty minOrderSizeProperty() {
        return minOrderSize;
    }

    public Symbol getSymbol() {
        return symbol;
    }
}
