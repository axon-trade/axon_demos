package io.xtrd.samples.sts.gui.md;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.fix.commads.SecuritiesListRequest;
import io.xtrd.samples.sts.gui.utils.FieldsHelper;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.controlsfx.control.SearchableComboBox;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class BrowserController implements Initializable {
    @Inject private Core core;
    @FXML SearchableComboBox<Exchange> exchangesSelector;
    @FXML TableView<SymbolRow> symbolsTable;
    @FXML TextField symbolNameField;
    @FXML Button sendButton;
    private IMarketDataProvider marketDataProvider = null;

    private ObservableList<SymbolRow> allSymbolsList = FXCollections.observableArrayList();
    private FilteredList<SymbolRow> filteredSymbolsList = new FilteredList<>(allSymbolsList, p -> true);
    private SortedList<SymbolRow> sortedSymbolsList = new SortedList<>(filteredSymbolsList);



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        exchangesSelector.setConverter(FieldsHelper.getStringConverter());
        exchangesSelector.getItems().addAll(core.getMarketDataModel().getExchanges());
        sendButton.setOnAction(this::onSend);
        initSymbolsTable();

        core.addSecuritiesListener(event -> allSymbolsList.add(new SymbolRow(event.getSymbol())));
    }

    private void initSymbolsTable() {
        symbolNameField.textProperty().addListener((obs, oldValue, newValue) -> {
            filteredSymbolsList.setPredicate(row -> {
                if(newValue == null || newValue.length() == 0) return true;
                if(row.nameProperty().get().toUpperCase().contains(newValue.toUpperCase())) return true;

                return false;
            });
        });

        sortedSymbolsList.comparatorProperty().bind(symbolsTable.comparatorProperty());

        symbolsTable.setItems(sortedSymbolsList);
        ContextMenu contextMenu = new ContextMenu();
        MenuItem subscribeItem = new MenuItem("Subscribe");
        subscribeItem.setOnAction(this::onSubscribe);
        contextMenu.getItems().add(subscribeItem);

        symbolsTable.setRowFactory(param -> {
            TableRow<SymbolRow> row = new TableRow<>();
            row.contextMenuProperty().bind(Bindings.when(Bindings.isNotNull(row.itemProperty())).then(contextMenu).otherwise((ContextMenu) null));

            return row;
        });
    }

    private void onSend(ActionEvent event) {
        allSymbolsList.clear();
        core.getSessionsManager().send(new SecuritiesListRequest(
                core.getNextId(),
                exchangesSelector.getValue(),
                1
        ));
    }

    private void onSubscribe(ActionEvent event) {
        SymbolRow row = symbolsTable.getSelectionModel().getSelectedItem();

        Symbol symbol = core.getMarketDataModel().addSymbol(row.getSymbol());
        marketDataProvider.subscribe(symbol);
    }

    public void setMarketDataProvider(IMarketDataProvider marketDataProvider) {
        this.marketDataProvider = marketDataProvider;
    }
}
