package io.xtrd.samples.sts.fix;

import biz.onixs.fix.dictionary.Version;
import biz.onixs.fix.engine.Session;
import biz.onixs.fix.engine.SslContextFactory;
import biz.onixs.fix.parser.Message;
import biz.onixs.fix.tag.FIX44;
import biz.onixs.fix.tag.Tag;
import io.xtrd.samples.common.fix.*;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.common.Command;
import io.xtrd.samples.sts.md.MarketDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.util.*;

public class SessionsManager {
    private final Logger logger = LoggerFactory.getLogger(SessionsManager.class);
    private Properties config;
    private Map<Integer, Credentials> credentialsMap = new HashMap<>();
    private Map<Integer, Session> sessionsMap = new HashMap<>();
    private Version fixVersion;
    private IConnectionStatusListener connectionStatusListener = null;
    private ISecurityListener securityListener = null;
    private IMarketDataListener marketDataListener = null;
    private ITradingSessionStatusListener tradingSessionStatusListener = null;
    private IPositionListener positionListener = null;
    private IExecutionReportListener executionReportListener = null;
    private MessagesFactory messagesFactory = null;
    private MessagesNormalizer messagesNormalizer = null;
    private MarketDataModel marketDataModel = null;

    private String getKey(int id, String key) {
        return new StringBuilder(Constants.Settings.SESSION_BASE).append(".").append(id).append(".").append(key).toString();
    }

    private Credentials.SessionType getSessionType(String type) throws Exception {
        switch (type) {
            case Constants.SessionTypes.MARKET_DATA:
                return Credentials.SessionType.MARKET_DATA;
            case Constants.SessionTypes.TRADING:
                return Credentials.SessionType.TRADING;
            default:
                throw new Exception("Unsupported session type '" + type + "'");
        }
    }

    public List<Credentials> getCredentials() {
        ArrayList<Credentials> result = new ArrayList<>(credentialsMap.values());
        result.sort((a, b) -> {
            if(a.getId() > b.getId()) {
                return 1;
            } else if (a.getId() < b.getId()) {
                return -1;
            } else {
                return 0;
            }
        });

        return result;
    }

    public void init() throws Exception {
        if(config == null) throw new Exception("Configuration is not provided");
        loadSessions();
        fixVersion = Version.getById(config.getProperty(Constants.Settings.DIALECT_NAME));
        messagesFactory = new MessagesFactory(fixVersion);
        messagesNormalizer = new MessagesNormalizer();
        messagesNormalizer.setMarketDataModel(marketDataModel);
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

    private void loadSessions() throws Exception {
        boolean loading = true;
        int id = 1;
        while(loading) {
            String tmp = config.getProperty(getKey(id, Constants.Settings.HOST));
            if(tmp == null) {
                loading = false;
            } else {
                Credentials credentials = new Credentials();
                credentials.setId(id);
                credentials.setHost(tmp);
                credentials.setPort(Integer.parseInt(config.getProperty(getKey(id, Constants.Settings.PORT))));
                credentials.setTargetCompId(config.getProperty(getKey(id, Constants.Settings.TARGET_COMP_ID)));
                credentials.setSenderCompId(config.getProperty(getKey(id, Constants.Settings.SENDER_COMP_ID)));
                credentials.setUsername(config.getProperty(getKey(id, Constants.Settings.USERNAME)));
                credentials.setPassword(config.getProperty(getKey(id, Constants.Settings.PASSWORD)));
                credentials.setHeartbeat(Integer.parseInt(config.getProperty(getKey(id, Constants.Settings.HEARTBEAT))));
                credentials.setUseSsl(Boolean.parseBoolean(config.getProperty(getKey(id, Constants.Settings.USE_SSL))));
                credentials.setSessionType(getSessionType(config.getProperty(getKey(id, Constants.Settings.SESSION_TYPE))));

                credentialsMap.put(credentials.getId(), credentials);

                id++;
            }
        }
    }

    public void setConnectionStatusListener(IConnectionStatusListener connectionStatusListener) {
        this.connectionStatusListener = connectionStatusListener;
    }

    public void setSecuritiesListener(ISecurityListener securityListener) {
        this.securityListener = securityListener;
    }

    public void setMarketDataListener(IMarketDataListener marketDataListener) {
        this.marketDataListener = marketDataListener;
    }

    public void setTradingSessionStatusListener(ITradingSessionStatusListener tradingSessionStatusListener) {
        this.tradingSessionStatusListener = tradingSessionStatusListener;
    }

    public void setPositionListener(IPositionListener positionListener) {
        this.positionListener = positionListener;
    }

    public void setExecutionReportListener(IExecutionReportListener executionReportListener) {
        this.executionReportListener = executionReportListener;
    }

    public void start(int sessionId) {
        if(logger.isDebugEnabled()) logger.debug("Starting Session ID: {}", sessionId);
        Credentials credentials = credentialsMap.get(sessionId);

        Session session = sessionsMap.get(sessionId);
        if(session == null) {
            Listener listener = new Listener(sessionId);
            listener.setConnectionStatusListener(connectionStatusListener);
            listener.setSecuritiesListener(securityListener);
            listener.setMarketDataListener(marketDataListener);
            listener.setMessagesNormalizer(messagesNormalizer);
            listener.setTradingSessionStatusListener(tradingSessionStatusListener);
            listener.setPositionListener(positionListener);
            listener.setExecutionReportListener(executionReportListener);


            session = new Session(credentials.getSenderCompId(), credentials.getTargetCompId(), fixVersion);
            session.setInboundApplicationMessageListener(listener);
            session.addStateChangeListener(listener);

            sessionsMap.put(sessionId, session);
        }

        Message logon = Message.create(FIX44.MsgType.Logon, fixVersion);
        logon.set(Tag.Username, credentials.getUsername());
        logon.set(Tag.Password, credentials.getPassword());
        logon.set(Tag.HeartBtInt, credentials.getHeartbeat());
        logon.set(Tag.ResetSeqNumFlag, "Y");//Important for Market data sessions

        if(credentials.isUseSsl()) {
            try {
                SSLContext sslContext = SslContextFactory.getInstance("xtrd", null, "pass33", "pass33", null);
                session.setSSLContext(sslContext);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        session.logonAsInitiator(credentials.getHost(), credentials.getPort(), logon);
    }

    public void stop(int sessionId) {
        if(logger.isDebugEnabled()) logger.debug("Stopping Session ID: {}", sessionId);
        Session session = sessionsMap.get(sessionId);
        if(session != null) session.logout("See you later");
    }

    public void send(Command cmd) {
        sessionsMap.get(cmd.getSessionId()).send(messagesFactory.getMessage(cmd));
    }

    public void setMarketDataModel(MarketDataModel marketDataModel) {
        this.marketDataModel = marketDataModel;
    }

}
