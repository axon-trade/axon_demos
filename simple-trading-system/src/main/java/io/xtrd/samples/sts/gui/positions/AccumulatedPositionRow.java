package io.xtrd.samples.sts.gui.positions;

import io.xtrd.samples.common.trade.Position;

import java.util.HashMap;
import java.util.Map;

public class AccumulatedPositionRow extends PositionRow {
    private Map<String, Double> sizes = new HashMap<>();

    public AccumulatedPositionRow(Position position) {
        super(position);

        sizes.put(exchange.getName(), position.getSize());
    }

    public void update(Position position) {
        sizes.put(position.getExchange().getName(), position.getSize());
        double newSize = 0;
        for(double size : sizes.values()) {
            newSize+=size;
        }

        this.size.set(newSize);
    }
}
