package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.Connector;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.Command;

import java.util.List;
import java.util.function.Consumer;

public class GetConnectors extends Command {
    private Account account;
    private Consumer<List<Connector>> consumer;

    public GetConnectors(long requestId, Account account, Consumer<List<Connector>>consumer) {
        super(requestId, -1);
        this.account = account;
        this.consumer = consumer;
    }

    public Account getAccount() {
        return account;
    }

    public Consumer<List<Connector>> getConsumer() {
        return consumer;
    }
}
