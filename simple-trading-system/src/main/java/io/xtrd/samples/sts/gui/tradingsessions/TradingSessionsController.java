package io.xtrd.samples.sts.gui.tradingsessions;

import io.xtrd.samples.common.fix.TradingSessionStatusEvent;
import io.xtrd.samples.sts.Core;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

import javax.inject.Inject;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class TradingSessionsController implements Initializable {
    @Inject private Core core;
    @FXML TableView<TradingSessionRow>statusesTable;
    @FXML TableColumn<TradingSessionRow, TradingSessionStatusEvent.Status>statusColumn;
    private Map<String, TradingSessionRow> rowsMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        core.addTradingSessionStatusListener(this::processTradingSessionStatus);

        statusColumn.setCellFactory(column -> new TableCell<TradingSessionRow, TradingSessionStatusEvent.Status>() {
            protected void updateItem(TradingSessionStatusEvent.Status item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : getItem().toString());
                setGraphic(null);
                TableRow<TradingSessionRow> row = getTableRow();
                if(!empty) {
                    //if(item) {
                        if(row != null && row.getItem() != null) {
                            switch (item) {
                                case CLOSED:
                                    row.setStyle("-fx-background-color:gainsboro");
                                    break;
                                case OPEN:
                                    row.setStyle("-fx-background-color:transparent");
                                    break;
                            }
                        } else {
                            row.setStyle("-fx-background-color:transparent");
                        }
                   // } else {
                    //    row.setStyle("-fx-background-color:lightcoral");
                   // }
                } else {
                    row.setStyle("-fx-background-color:transparent");
                }
            }
        });
    }

    private void processTradingSessionStatus(TradingSessionStatusEvent event) {
        TradingSessionRow row = rowsMap.get(getKey(event));
        if(row == null) {
            row = new TradingSessionRow(event);
            rowsMap.put(getKey(event), row);

            statusesTable.getItems().add(row);
        } else {
            row.update(event);
        }

    }

    private String getKey(TradingSessionStatusEvent event) {
        return new StringBuilder(event.getAccount().getId()).append("_").append(event.getExchange().getName()).toString();
    }
}
