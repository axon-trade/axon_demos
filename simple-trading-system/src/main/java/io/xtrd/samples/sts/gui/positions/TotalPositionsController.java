package io.xtrd.samples.sts.gui.positions;

import io.xtrd.samples.common.trade.Position;
import io.xtrd.samples.sts.Core;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;

import javax.inject.Inject;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class TotalPositionsController implements Initializable {
    @Inject private Core core;
    @FXML TableView<AccumulatedPositionRow> positionsTable;
    private Map<String, AccumulatedPositionRow> positions = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        core.addPositionListener(this::onPosition);
    }

    private void onPosition(Position position) {
        AccumulatedPositionRow row = positions.get(position.getAsset().getName());
        if(row == null) {
            row = new AccumulatedPositionRow(position);
            positions.put(position.getAsset().getName(), row);
            positionsTable.getItems().add(row);
        } else {
            row.update(position);
        }
    }
}
