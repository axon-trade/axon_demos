package io.xtrd.samples.sts.gui.positions;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.trade.Position;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.gui.utils.FieldsHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;

import javax.inject.Inject;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class FilteredPositionsController implements Initializable {
    @Inject private Core core;
    @FXML TableView<PositionRow> positionsTable;
    @FXML ChoiceBox<Account>accountSelector;
    private Map<Integer, Map<String, PositionRow>>rows = new HashMap<>();//login id -> map<exchange:asset, position>

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        accountSelector.setConverter(FieldsHelper.getAccountStringConverter());
        accountSelector.getItems().addAll(core.getOrganizationsModel().getAccounts());
        accountSelector.getSelectionModel().selectedItemProperty().addListener((obs, old, newValue) -> {
            positionsTable.getItems().clear();
            Login login = core.getOrganizationsModel().getLogins(newValue.getId()).get(0);
            Map<String, PositionRow>loginPositions = rows.get(login.getId());
            if(loginPositions != null) positionsTable.getItems().addAll(loginPositions.values());
        });

        core.addPositionListener(this::onPosition);
    }

    private void onPosition(Position position) {
        Map<String, PositionRow>loginPositions = rows.get(position.getLogin().getId());
        if(loginPositions == null) {
            PositionRow row = new PositionRow(position);
            loginPositions = new HashMap<>();
            loginPositions.put(getKey(position), row);

            rows.put(position.getLogin().getId(), loginPositions);

            validateSelection(row, position);
        } else {
            PositionRow row = loginPositions.get(getKey(position));
            if(row == null) {
                row = new PositionRow(position);
                loginPositions.put(getKey(position), row);

                validateSelection(row, position);
            } else {
                row.update(position);
            }
        }
    }

    private void validateSelection(PositionRow row, Position position) {
        Account selectedAccount = accountSelector.getValue();
        if(selectedAccount != null) {
            Login login = core.getOrganizationsModel().getLogins(selectedAccount.getId()).get(0);
            if(login.getId() == position.getLogin().getId()) {
                positionsTable.getItems().add(row);
            }
        }

    }

    private String getKey(Position position) {
        return new StringBuilder(position.getExchange().getName()).append("_").append(position.getAsset().getName()).toString();
    }
}
