package io.xtrd.samples.sts.rest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.xtrd.samples.common.Connector;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.org.Organization;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.common.Command;
import io.xtrd.samples.sts.rest.commands.*;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class MessagesNormalizer {
    private Gson gson = new Gson();

    public void parse(Command cmd, String json) {
        if(cmd instanceof GetAccounts) {
            parseAccounts((GetAccounts) cmd, json);
        } else if (cmd instanceof GetLogins) {
            parseLogins((GetLogins) cmd, json);
        } else if (cmd instanceof GetConnectors) {
            parseConnectors((GetConnectors) cmd, json);
        } else if (cmd instanceof AddAccount) {
            parseAddAccount((AddAccount) cmd, json);
        } else if (cmd instanceof ChangeProperty) {
            parseChangeProperty((ChangeProperty) cmd, json);
        } else if (cmd instanceof DeleteAccount) {
            parseDeleteAccount((DeleteAccount) cmd, json);
        } else if (cmd instanceof GetRoutes) {
            parseGetRoutes((GetRoutes) cmd, json);
        } else if (cmd instanceof GetRouteSettings) {
            parseRouteSettings((GetRouteSettings) cmd, json);
        } else if (cmd instanceof AddConnector) {
            parseAddConnector((AddConnector) cmd, json);
        } else if (cmd instanceof GetOrganization) {
            parseGetOrganization((GetOrganization) cmd, json);
        } else if (cmd instanceof GetExchanges) {
            parseGetExchanges((GetExchanges) cmd, json);
        }
    }

    private void parseGetExchanges(GetExchanges cmd, String json) {
        JsonArray array = gson.fromJson(json, JsonArray.class);
        List<Exchange> result = new ArrayList<>();
        for(int i = 0;i < array.size();i++) {
            result.add(parseExchange(array.get(i).getAsJsonObject()));
        }
        cmd.getConsumer().accept(result);
    }

    private void parseGetOrganization(GetOrganization cmd, String json) {
        JsonObject obj = gson.fromJson(json, JsonObject.class);

        Organization result = new Organization(
            obj.get(Constants.Api.ID).getAsInt(),
            obj.get(Constants.Api.NAME).getAsString()
        );
        cmd.getConsumer().accept(result);
    }

    private void parseAddConnector(AddConnector cmd, String json) {
        JsonObject obj = gson.fromJson(json, JsonObject.class);
        Connector result = new Connector();
        result.setId(obj.get(Constants.Api.ID).getAsInt());
        result.setName(obj.get(Constants.Api.NAME).getAsString());
        result.setStatus(Connector.Status.ACTIVE);

        cmd.getConsumer().accept(result);
    }

    private void parseRouteSettings(GetRouteSettings cmd, String json) {
        JsonObject obj = gson.fromJson(json, JsonObject.class);
        JsonArray settings = obj.getAsJsonArray(Constants.Api.SETTINGS);
        ArrayList<Pair<String, String>>result = new ArrayList<>();
        for(int i = 0;i < settings.size();i++) {
            JsonObject record = settings.get(i).getAsJsonObject();
            result.add(new Pair<>(record.get(Constants.Api.KEY).getAsString(), null));
        }

        cmd.getConsumer().accept(result);
    }

    private void parseGetRoutes(GetRoutes cmd, String json) {
        JsonArray array = gson.fromJson(json, JsonArray.class);
        ArrayList<Exchange>result = new ArrayList<>();

        for(int i = 0;i < array.size();i++) {
            result.add(parseExchange(array.get(i).getAsJsonObject()));
        }

        cmd.getConsumer().accept(result);
    }

    private Exchange parseExchange(JsonObject obj) {
        return new Exchange.Builder()
                .id(obj.get(Constants.Api.ID).getAsInt())
                .name(obj.get(Constants.Api.NAME).getAsString())
                .fullName(obj.has(Constants.Api.FULL_NAME) ? obj.get(Constants.Api.FULL_NAME).getAsString() : obj.get(Constants.Api.NAME).getAsString())
                .build();
    }

    private void parseDeleteAccount(DeleteAccount cmd, String json) {
        cmd.getConsumer().accept(Boolean.TRUE);
    }

    private void parseChangeProperty(ChangeProperty cmd, String json) {
        cmd.getConsumer().accept(Boolean.TRUE);
    }

    private void parseAddAccount(AddAccount cmd, String json) {
        JsonObject obj = gson.fromJson(json, JsonObject.class);
        Account account = new Account(
            obj.get(Constants.Api.ID).getAsInt(),
            obj.get(Constants.Api.NAME).getAsString()
        );
        account.setOrganizationId(obj.get(Constants.Api.ORGANIZATION_ID).getAsInt());

        Login login = parseLogin(obj.getAsJsonObject(Constants.Api.LOGIN), account);

        cmd.getConsumer().accept(account, login);
    }

    private void parseConnectors(GetConnectors cmd, String json) {
        JsonArray array = gson.fromJson(json, JsonArray.class);
        ArrayList<Connector>result = new ArrayList<>();
        for(int i = 0;i < array.size();i++) {
            result.add(parseConnector(array.get(i).getAsJsonObject()));
        }

        cmd.getConsumer().accept(result);
    }

    private Connector parseConnector(JsonObject obj) {
        Connector result = new Connector();
        result.setId(obj.get(Constants.Api.ID).getAsInt());
        result.setName(obj.get(Constants.Api.NAME).getAsString());
        result.setDescription(obj.get(Constants.Api.DESCRIPTION).getAsString());
        result.setStatus(Connector.Status.fromInt(obj.get(Constants.Api.STATUS).getAsInt()));

        return result;
    }

    private void parseAccounts(GetAccounts cmd, String json) {
        JsonArray array = gson.fromJson(json, JsonArray.class);
        ArrayList<Account>result = new ArrayList<>();
        for(int i = 0;i < array.size();i++) {
            result.add(parseAccount(array.get(i).getAsJsonObject()));
        }

        cmd.getConsumer().accept(result);
    }

    private Account parseAccount(JsonObject obj) {
        return new Account(
            obj.get(Constants.Api.ID).getAsInt(),
            obj.get(Constants.Api.NAME).getAsString()
        );
    }

    private void parseLogins(GetLogins cmd, String json) {
        JsonArray array = gson.fromJson(json, JsonArray.class);
        ArrayList<Login>result = new ArrayList<>();
        for(int i = 0;i < array.size();i++) {
            result.add(parseLogin(array.get(i).getAsJsonObject(), cmd.getAccount()));
        }

        cmd.getConsumer().accept(result);
    }

    private Login parseLogin(JsonObject obj, Account account) {
        Login result = new Login(
            obj.get(Constants.Api.ID).getAsInt(),
            obj.get(Constants.Api.NAME).getAsString()
        );
        result.setAccountId(account.getId());

        return result;
    }

}
