package io.xtrd.samples.sts.fix.commads;

import io.xtrd.samples.common.Command;

public class TradingSessionStatusRequest extends Command {
    public TradingSessionStatusRequest(long requestId, int sessionId) {
        super(requestId, sessionId);
    }
}
