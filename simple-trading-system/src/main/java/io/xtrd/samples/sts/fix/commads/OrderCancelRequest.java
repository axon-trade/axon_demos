package io.xtrd.samples.sts.fix.commads;

import io.xtrd.samples.common.Command;
import io.xtrd.samples.common.trade.Order;

public class OrderCancelRequest extends Command {
    private Order order;

    public OrderCancelRequest(long requestId, int sessionId, Order order) {
        super(requestId, sessionId);
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}
