package io.xtrd.samples.sts.gui.management.data;


import io.xtrd.samples.common.org.Account;

public class AccountEntity extends BaseEntity<Account> {
    public AccountEntity(Account account) {
        super(account);
        name.setValue(account.getName());
    }
}
