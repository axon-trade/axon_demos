package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.common.trade.Order;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class UpdateController implements Initializable {
    @FXML
    TextField sizeField;
    @FXML TextField priceField;
    @FXML Button sendButton;
    @FXML Button cancelButton;

    private ButtonType action;
    private Order order;

    public ButtonType getButtonType() {
        return action;
    }

    public void setOrder(Order order) {
        this.order = order;
        sizeField.setText("" + order.getSize());
        priceField.setText("" + order.getPrice());
    }

    public Order getOrder() {
        order.setPrice(Double.parseDouble(priceField.getText()));
        order.setSize(Double.parseDouble(sizeField.getText()));

        return order;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sendButton.setOnAction(e -> {
            action = ButtonType.OK;
            closeWindow((Node) e.getSource());
        });

        cancelButton.setOnAction(e -> {
            action = ButtonType.CANCEL;
            closeWindow((Node) e.getSource());
        });
    }

    private void closeWindow(Node source) {
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
