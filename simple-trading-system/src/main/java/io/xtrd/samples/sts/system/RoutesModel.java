package io.xtrd.samples.sts.system;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.sts.rest.ApiClient;
import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class RoutesModel {
    private ApiClient apiClient;
    private AtomicLong idGenerator = new AtomicLong(0);
    private List<Exchange> routes = new ArrayList<>();
    private Map<Integer, List<Pair<String, String>>> routesSettings = new HashMap<>();
    private Properties config;
    private long syncTimeout;
    private int routerId = 1;

    public List<Exchange>getRoutes() {
        return routes;
    }

    public List<Pair<String, String>>getSettings(int routeId) {
        return routesSettings.get(routeId);
    }

    public void init() throws Exception {
        syncTimeout = Long.parseLong(config.getProperty(Constants.Settings.SYNC_TIMEOUT, "5000"));
        CountDownLatch routesLatch = new CountDownLatch(1);
        apiClient.getRoutes(idGenerator.incrementAndGet(), routerId, routes -> {
            this.routes.addAll(routes);
            this.routes.sort(Comparator.comparing(Exchange::getName));
            routesLatch.countDown();
        });
        if(!routesLatch.await(syncTimeout, TimeUnit.MILLISECONDS)) throw new Exception("Timeout during Routes synchronization");
        CountDownLatch routeSettingsLatch = new CountDownLatch(routes.size());
        for(Exchange exchange : routes) {
            routesSettings.put(exchange.getId(), new ArrayList<>());
            apiClient.getRouteSettings(idGenerator.incrementAndGet(), exchange.getId(), settings -> {
                routesSettings.get(exchange.getId()).addAll(settings);
                routeSettingsLatch.countDown();
            });
        }
        if(!routeSettingsLatch.await(syncTimeout, TimeUnit.MILLISECONDS)) throw new Exception("Timeout during Routes settings synchronization");
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

}
