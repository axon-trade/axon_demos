package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.sts.Core;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class ReportsController implements Initializable {
    @Inject private Core core;
    @FXML TableView<ExecutionReportRow>tradesTable;
    @FXML TableView<ExecutionReportRow>allReportsTable;


    private ObservableList<ExecutionReportRow> allReports = FXCollections.observableArrayList();
    private FilteredList<ExecutionReportRow> tradesList = new FilteredList<>(allReports, report -> {
        switch (report.statusProperty().get()) {
            case PARTIALLY_FILLED:
            case FILLED:
                return true;
            default:
                return false;
        }
    });


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        core.addExecutionReportListener(report -> allReports.add(new ExecutionReportRow(report)));
        tradesTable.setItems(tradesList);
        allReportsTable.setItems(allReports);
    }
}
