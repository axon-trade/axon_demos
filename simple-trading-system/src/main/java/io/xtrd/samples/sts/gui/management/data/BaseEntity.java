package io.xtrd.samples.sts.gui.management.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

abstract public class BaseEntity<T> {
    private T entity;
    protected StringProperty name = new SimpleStringProperty();

    public BaseEntity(T entity) {
        this.entity = entity;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public T getEntity() {
        return entity;
    }


}
