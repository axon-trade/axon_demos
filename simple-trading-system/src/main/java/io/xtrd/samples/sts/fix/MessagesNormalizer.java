package io.xtrd.samples.sts.fix;

import biz.onixs.fix.parser.Group;
import biz.onixs.fix.parser.Message;
import biz.onixs.fix.tag.FIX44;
import biz.onixs.fix.tag.Tag;
import io.xtrd.samples.common.fix.SecurityListEvent;
import io.xtrd.samples.common.fix.TradingSessionStatusEvent;
import io.xtrd.samples.common.md.Asset;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.MarketDataEvent;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.trade.ExecutionReport;
import io.xtrd.samples.common.trade.Order;
import io.xtrd.samples.common.trade.Position;
import io.xtrd.samples.sts.md.MarketDataModel;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class MessagesNormalizer {
    private MarketDataModel marketDataModel = null;
    private final DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYYMMdd-HH:mm:ss.SSS");

    private MarketDataEvent.Side getMdEventSide(String value) {
        switch (value) {
            case FIX44.MDEntryType.Bid:
                return MarketDataEvent.Side.BID;
            case FIX44.MDEntryType.Offer:
                return MarketDataEvent.Side.ASK;
            default:
                return MarketDataEvent.Side.UNDEFINED;
        }
    }

    public void setMarketDataModel(MarketDataModel marketDataModel) {
        this.marketDataModel = marketDataModel;
    }

    public SecurityListEvent parseSecurityList(Message msg) {
        Group symbolsGroup = msg.getGroup(Tag.NoRelatedSym);
        Exchange exchange = new Exchange.Builder().name(symbolsGroup.get(Tag.SecurityExchange, 0)).build();
        Symbol symbol = new Symbol.Builder()
            .exchange(exchange)
            .name(symbolsGroup.get(Tag.Symbol, 0))
            .pricePower(symbolsGroup.getInteger(5001, 0))
            .sizePower(symbolsGroup.getInteger(5002, 0))
            .minOrderSize(symbolsGroup.contains(Tag.MinTradeVol, 0) ? symbolsGroup.getDouble(Tag.MinTradeVol, 0) : 0)
            .build();

        return new SecurityListEvent(
            msg.getLong(Tag.SecurityReqID),
            symbol,
            msg.getInteger(Tag.TotNoRelatedSym),
            msg.hasFlag(Tag.LastFragment));

    }

    public TradingSessionStatusEvent parseTradingSessionStatus(Message msg) {
        Exchange exchange = new Exchange.Builder().name(msg.get(Tag.TradingSessionID)).build();
        Group accountGroup = msg.getGroup(Tag.NoPartyIDs);

        Group accountInfo = accountGroup.getGroup(Tag.NoPartySubIDs, 0);
        Account account = new Account();
        for(int i = 0;i < accountGroup.getInteger(Tag.NoPartySubIDs, 0);i++) {
            switch (accountInfo.get(Tag.PartySubIDType, i)) {
                case FIX44.PartySubIDType.SecuritiesAccountNumber:
                    account.setId(accountInfo.getInteger(Tag.PartySubID, i));
                    break;
                case FIX44.PartySubIDType.SecuritiesAccountName:
                    account.setName(accountInfo.get(Tag.PartySubID, i));
                    break;
            }
        }
        TradingSessionStatusEvent.Status status;
        String text = null;
        switch (msg.get(Tag.TradSesStatus)) {
            case FIX44.TradSesStatus.Open:
                status = TradingSessionStatusEvent.Status.OPEN;
                break;
            case FIX44.TradSesStatus.Closed:
                status = TradingSessionStatusEvent.Status.CLOSED;
                if(msg.contains(Tag.Text)) text = msg.get(Tag.Text);
                break;
            default:
                status = null;
        }

        return new TradingSessionStatusEvent(account, exchange, status, text);
    }

    public MarketDataEvent[] parseMarketDataSnapshot(Message msg) {
        Symbol symbol = marketDataModel.getSymbol(msg.get(Tag.SecurityExchange), msg.get(Tag.Symbol));

        int size = msg.getInteger(Tag.NoMDEntries);

        MarketDataEvent events[] = new MarketDataEvent[size];

        Group entries = msg.getGroup(Tag.NoMDEntries);

        DateTime timestamp;
        for(int i = 0;i < size;i++) {
            timestamp = fmt.parseDateTime(entries.get(Tag.MDEntryDate, i) + "-" + entries.get(Tag.MDEntryTime, i)).withZone(DateTimeZone.UTC);

            events[i] = new MarketDataEvent(
                symbol.getId(),
                MarketDataEvent.UpdateType.NEW,
                getMdEventSide(entries.get(Tag.MDEntryType, i)),
                Symbol.castToLong(entries.getDouble(Tag.MDEntryPx, i), symbol.getPriceFactor()),
                Symbol.castToLong(entries.getDouble(Tag.MDEntrySize, i), symbol.getSizeFactor()),
                timestamp.getMillis()
            );
        }

        return events;
    }

    public MarketDataEvent[] parseMarketDataIncrementalRefresh(Message msg) {
        Symbol symbol = marketDataModel.getSymbol(msg.get(Tag.SecurityExchange), msg.get(Tag.Symbol));
        int size = msg.getInteger(Tag.NoMDEntries);

        MarketDataEvent events[] = new MarketDataEvent[size];
        Group entries = msg.getGroup(Tag.NoMDEntries);

        DateTime timestamp;
        MarketDataEvent.UpdateType type = MarketDataEvent.UpdateType.UNDEFINED;
        long priceLevelSize = 0;
        for(int i = 0;i < size;i++) {
            timestamp = fmt.parseDateTime(entries.get(Tag.MDEntryDate, i) + "-" + entries.get(Tag.MDEntryTime, i)).withZone(DateTimeZone.UTC);

            switch (entries.get(Tag.MDUpdateAction, i)) {
                case FIX44.MDUpdateAction.New:
                    type = MarketDataEvent.UpdateType.NEW;
                    break;
                case FIX44.MDUpdateAction.Change:
                    type = MarketDataEvent.UpdateType.UPDATE;
                    break;
                case FIX44.MDUpdateAction.Delete:
                    type = MarketDataEvent.UpdateType.DELETE;
                    break;
            }

            if(type != MarketDataEvent.UpdateType.DELETE) {
                //XTRD will not stream size for DELETE actions
                priceLevelSize = Symbol.castToLong(entries.getDouble(Tag.MDEntrySize, i), symbol.getSizeFactor());
            } else {
                priceLevelSize = 0;
            }

            events[i] = new MarketDataEvent(
                symbol.getId(),
                type,
                getMdEventSide(entries.get(Tag.MDEntryType, i)),
                Symbol.castToLong(entries.getDouble(Tag.MDEntryPx, i), symbol.getPriceFactor()),
                priceLevelSize,
                timestamp.getMillis()
            );
        }

        return events;
    }

    public Position parsePositionReport(Message msg) {
        Login login = null;
        Exchange exchange = null;

        Group group = msg.getGroup(Tag.NoPartyIDs);
        for(int i = 0;i < msg.getInteger(Tag.NoPartyIDs);i++) {
            switch (group.get(Tag.PartyRole, i)) {
                case FIX44.PartyRole.InitiatingTrader:
                    login = new Login(group.getInteger(Tag.PartyID, i), "");
                    break;
                case FIX44.PartyRole.Exchange:
                    exchange = new Exchange.Builder().name(group.get(Tag.PartyID, i)).build();
                    break;
            }
        }

        Asset asset = new Asset(msg.get(Tag.Symbol));

        Group noPositions = msg.getGroup(Tag.NoPositions);
        if(noPositions.contains(Tag.LongQty, 0)) {
            return new Position(login, asset, exchange, noPositions.getDouble(Tag.LongQty, 0));
        } else {
            return new Position(login, asset, exchange, -noPositions.getDouble(Tag.ShortQty, 0));
        }
    }

    public ExecutionReport parseExecutionReport(Message msg) {
        ExecutionReport result = new ExecutionReport();
        result.setId(msg.getInteger(Tag.ClOrdID));
        result.setReceiveTime(getTimestamp());
        result.setTransactionTime(fmt.withZoneUTC().parseDateTime(msg.get(Tag.TransactTime)));

        Symbol symbol = marketDataModel.getSymbol(msg.get(Tag.ExDestination), msg.get(Tag.Symbol));
        result.setSymbol(symbol);
        result.setExchange(new Exchange.Builder().name(msg.get(Tag.ExDestination)).build());

        String status = msg.get(Tag.OrdStatus);
        switch (status) {
            case FIX44.OrdStatus.PendingNew:
                result.setOrderId(msg.getLong(Tag.OrderID));
                result.setStatus(Order.Status.PENDING_NEW);
                break;
            case FIX44.OrdStatus.New:
                result.setStatus(Order.Status.NEW);
                result.setOrderId(msg.getLong(Tag.OrderID));
                result.setSecondaryOrderId(msg.get(Tag.SecondaryOrderID));
                break;
            case FIX44.OrdStatus.Partial:
                result.setOrderId(msg.getLong(Tag.OrderID));
                result.setSecondaryOrderId(msg.get(Tag.SecondaryOrderID));
                result.setStatus(Order.Status.PARTIALLY_FILLED);
                result.setLastPrice(msg.getDouble(Tag.LastPx));
                result.setLastSize(msg.getDouble(Tag.LastQty));
                result.setFilledSize(msg.getDouble(Tag.CumQty));
                result.setCommissionSize(msg.getDouble(Tag.Commission));
                result.setCommissionAsset(new Asset(msg.get(Tag.CommCurrency)));
                break;
            case FIX44.OrdStatus.Filled:
                result.setOrderId(msg.getLong(Tag.OrderID));
                result.setSecondaryOrderId(msg.get(Tag.SecondaryOrderID));
                result.setStatus(Order.Status.FILLED);
                result.setLastPrice(msg.getDouble(Tag.LastPx));
                result.setLastSize(msg.getDouble(Tag.LastQty));
                result.setFilledSize(msg.getDouble(Tag.CumQty));
                result.setCommissionSize(msg.getDouble(Tag.Commission));
                result.setCommissionAsset(new Asset(msg.get(Tag.CommCurrency)));
                break;
            case FIX44.OrdStatus.Canceled:
                result.setStatus(Order.Status.CANCELED);
                if(msg.contains(Tag.OrigClOrdID)) {
                    //This is confirmed cancel order
                    result.setOriginalId(msg.getInteger(Tag.OrigClOrdID));
                    result.setOrderId(msg.getLong(Tag.OrderID));
                    result.setSecondaryOrderId(msg.get(Tag.SecondaryOrderID));
                }
                break;
            case FIX44.OrdStatus.Rejected:
                result.setOrderId(-1);
                result.setStatus(Order.Status.REJECTED);
                result.setText(msg.get(Tag.Text));
                break;
            case FIX44.OrdStatus.PendingRep:
                result.setStatus(Order.Status.PENDING_REPLACE);
                break;
        }

        return result;
    }

    private DateTime getTimestamp() {
        return new DateTime().withZone(DateTimeZone.UTC);
    }
}
