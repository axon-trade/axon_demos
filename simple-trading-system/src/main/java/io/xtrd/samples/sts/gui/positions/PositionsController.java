package io.xtrd.samples.sts.gui.positions;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.fix.commads.PositionsRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javax.inject.Inject;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PositionsController implements Initializable {
    @Inject private Core core;
    @FXML Button sendButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sendButton.setOnAction(this::onSend);
    }

    private void onSend(ActionEvent event) {
        List<Login> logins = new ArrayList<>();
        for(Account account : core.getOrganizationsModel().getAccounts()) {
            List<Login>accountLogins = core.getOrganizationsModel().getLogins(account.getId());
            if(accountLogins != null) {
                logins.addAll(accountLogins);
            } else {
                System.out.println("Can't find logins for Account '" + account.getName() + "'");
            }
        }
        core.getSessionsManager().send(new PositionsRequest(
            core.getNextId(),
            2,
            core.getOrganization(),
            logins
        ));
    }
}
