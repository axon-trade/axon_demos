package io.xtrd.samples.sts.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.sts.Constants;
import javafx.util.Pair;

import java.util.List;

public class MessagesFactory {
    public String getAddAccount(Account account) {
        JsonObject result = new JsonObject();
        result.addProperty(Constants.Api.NAME, account.getName());
        result.addProperty(Constants.Api.ORGANIZATION_ID, account.getOrganizationId());
        result.addProperty(Constants.Api.STATUS, "1");

        return result.toString();
    }

    public String getProperties(List<Pair<String, String>> pairs) {
        return serializeProperties(pairs).toString();
    }

    public String addConnector(Exchange exchange, List<Pair<String, String>>settings) {
        JsonObject result = new JsonObject();
        result.addProperty(Constants.Api.ROUTE_ID, exchange.getId());
        result.addProperty(Constants.Api.NAME, exchange.getName());
        result.addProperty(Constants.Api.DESCRIPTION, exchange.getName());

        JsonArray params = serializeProperties(settings);
        result.add(Constants.Api.PARAMS, params);

        return result.toString();
    }

    private JsonArray serializeProperties(List<Pair<String, String>>pairs) {
        JsonArray result = new JsonArray();

        for(Pair<String, String>pair : pairs) {
            JsonObject obj = new JsonObject();

            obj.addProperty(Constants.Api.KEY, pair.getKey());
            obj.addProperty(Constants.Api.VALUE, pair.getValue());

            result.add(obj);
        }

        return result;
    }
}
