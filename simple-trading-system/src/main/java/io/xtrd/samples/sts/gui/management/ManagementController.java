package io.xtrd.samples.sts.gui.management;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.gui.management.data.AccountEntity;
import io.xtrd.samples.sts.gui.management.data.BaseEntity;
import io.xtrd.samples.sts.gui.management.data.ConnectorEntity;
import io.xtrd.samples.sts.gui.management.data.OrganizationEntity;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import javax.inject.Inject;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class ManagementController implements Initializable {
    @Inject private Core core;
    @FXML TreeView<BaseEntity> organizationView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TreeItem<BaseEntity>root = new TreeItem<>(new OrganizationEntity(core.getOrganization()));

        core.getOrganizationsModel().getAccounts().forEach(account -> {
            TreeItem<BaseEntity>accountEntity = new TreeItem<>(new AccountEntity(account));
            core.getOrganizationsModel().getConnectors(account.getId()).forEach(connector -> {
                accountEntity.getChildren().add(new TreeItem(new ConnectorEntity(connector)));
            });
            root.getChildren().add(accountEntity);
        });

        ContextMenu organizationContextMenu = getOrganizationContextMenu();
        ContextMenu accountContextMenu = getAccountContextMenu();
        ContextMenu connectorContextMenu = getConnectorContextMenu();

        organizationView.setCellFactory(new Callback<TreeView<BaseEntity>, TreeCell<BaseEntity>>() {
            @Override
            public TreeCell<BaseEntity> call(TreeView<BaseEntity> param) {
                TreeCell<BaseEntity>cell = new TreeCell<BaseEntity>(){
                    protected void updateItem(BaseEntity item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) {
                            setText(null);
                        } else {
                            setText(item.nameProperty().getValue());

                            if(item instanceof AccountEntity) {
                                setContextMenu(accountContextMenu);
                            } else if (item instanceof OrganizationEntity) {
                                setContextMenu(organizationContextMenu);
                            } else if (item instanceof ConnectorEntity) {
                                setContextMenu(connectorContextMenu);
                            }

                        }
                    }
                };

                return cell;
            }
        });

        organizationView.setRoot(root);
        organizationView.setShowRoot(true);
    }

    private ContextMenu getOrganizationContextMenu() {
        ContextMenu result = new ContextMenu();
        MenuItem addAccountItem = new MenuItem("Add Account");
        addAccountItem.setOnAction(this::onAddAccount);
        result.getItems().add(addAccountItem);

        return result;
    }

    private ContextMenu getAccountContextMenu() {
        ContextMenu result = new ContextMenu();
        MenuItem addExchangeItem = new MenuItem("Add Exchange");
        addExchangeItem.setOnAction(this::onAddExchange);
        MenuItem deleteAccountItem = new MenuItem("Delete");
        deleteAccountItem.setOnAction(this::onDeleteAccount);

        result.getItems().addAll(addExchangeItem, deleteAccountItem);

        return result;
    }

    private ContextMenu getConnectorContextMenu() {
        ContextMenu result = new ContextMenu();
        MenuItem startConnectorItem = new MenuItem("Start");
        startConnectorItem.setOnAction(this::onConnectorStart);

        MenuItem stopConnectorItem = new MenuItem("Stop");
        stopConnectorItem.setOnAction(this::onConnectorStop);

        MenuItem deleteConnectorItem = new MenuItem("Delete");
        deleteConnectorItem.setOnAction(this::onConnectorDelete);

        result.getItems().addAll(startConnectorItem, stopConnectorItem, deleteConnectorItem);

        return result;
    }

    private void onAddExchange(ActionEvent event) {
        ConnectorView view = new ConnectorView();
        Scene scene = new Scene(view.getView());
        Stage stage = new Stage();
        stage.initModality(Modality.NONE);
        stage.setScene(scene);
        stage.setTitle("Add new Connector");


        stage.showAndWait();
        ConnectorController controller = ((ConnectorController) view.getPresenter());
        if(!controller.getSettings().isEmpty()) {
            AccountEntity entity = (AccountEntity) organizationView.getSelectionModel().getSelectedItem().getValue();
            core.getOrganizationsModel().addConnector(
                    entity.getEntity(),
                    controller.getExchange(),
                    controller.getSettings(),
                    connector -> {
                        TreeItem<BaseEntity>connectorEntity = new TreeItem<>(new ConnectorEntity(connector));
                        organizationView.getSelectionModel().getSelectedItem().getChildren().add(connectorEntity);
                    }
            );
        }
    }

    private void onConnectorStart(ActionEvent event) {

    }

    private void onConnectorStop(ActionEvent event) {

    }

    private void onConnectorDelete(ActionEvent e) {

    }



    private void onAddAccount(ActionEvent event) {
        //Account account = ((AccountEntity) orgTree.getSelectionModel().getSelectedItem().getValue()).getEntity();
        Dialog<String>dialog = new Dialog<>();
        dialog.setTitle("Add Account");
        dialog.setHeaderText("Add Login for Organization '" + core.getOrganization().getName() + "'");
        ButtonType createType = new ButtonType("ADD", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createType, ButtonType.CANCEL);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setPromptText("Name");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> name.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createType) {
                return name.getText();
            }
            return null;
        });

        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            Account account = new Account(result.get());
            account.setOrganizationId(core.getOrganization().getId());
            core.getOrganizationsModel().addAccount(account, newAccount -> {
                TreeItem<BaseEntity>accountEntity = new TreeItem<>(new AccountEntity(account));
                organizationView.getRoot().getChildren().add(accountEntity);
            });
        }
    }

    private void onDeleteAccount(ActionEvent event) {
        AccountEntity entity = (AccountEntity) organizationView.getSelectionModel().getSelectedItem().getValue();
        core.getOrganizationsModel().deleteAccount(entity.getEntity(), ack -> {
            organizationView.getRoot().getChildren().remove(organizationView.getSelectionModel().getSelectedItem());
        });
    }
}
