package io.xtrd.samples.sts.rest.commands;

import io.xtrd.samples.common.Command;
import javafx.util.Pair;

import java.util.List;
import java.util.function.Consumer;

public class GetRouteSettings extends Command {
    private Consumer<List<Pair<String, String>>>consumer;

    public GetRouteSettings(long requestId, Consumer<List<Pair<String, String>>> consumer) {
        super(requestId, -1);
        this.consumer = consumer;
    }

    public Consumer<List<Pair<String, String>>> getConsumer() {
        return consumer;
    }
}
