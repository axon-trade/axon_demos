package io.xtrd.samples.sts.gui.md;

import io.xtrd.samples.common.md.Symbol;

@FunctionalInterface
public interface IMarketDataProvider {
    void subscribe(Symbol symbol);
}
