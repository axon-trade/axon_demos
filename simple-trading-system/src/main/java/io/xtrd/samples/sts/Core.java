package io.xtrd.samples.sts;

import io.xtrd.samples.common.fix.*;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.org.Organization;
import io.xtrd.samples.sts.fix.SessionsManager;
import io.xtrd.samples.sts.fix.commads.PositionsRequest;
import io.xtrd.samples.sts.fix.commads.TradingSessionStatusRequest;
import io.xtrd.samples.sts.md.MarketDataModel;
import io.xtrd.samples.sts.rest.ApiClient;
import io.xtrd.samples.sts.org.OrganizationsModel;
import io.xtrd.samples.sts.system.RoutesModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

public class Core {
    private Properties config = null;
    private MarketDataModel marketDataModel;
    private OrganizationsModel organizationsModel = null;
    private RoutesModel routesModel = null;
    private SessionsManager sessionsManager = null;
    private ApiClient apiClient;
    private List<IConnectionStatusListener> connectionStatusListeners = new ArrayList<>();
    private List<ISecurityListener> securitiesListeners = new ArrayList<>();
    private List<IMarketDataListener>marketDataListeners = new ArrayList<>();
    private List<ITradingSessionStatusListener>tradingSessionStatusListeners = new ArrayList<>();
    private List<IPositionListener>positionListeners = new ArrayList<>();
    private List<IExecutionReportListener>executionReportListeners = new ArrayList<>();
    private AtomicLong idGenerator = new AtomicLong(0);
    private Organization organization;

    public void addConnectionStatusListener(IConnectionStatusListener listener) {
        if(!connectionStatusListeners.contains(listener)) connectionStatusListeners.add(listener);
    }

    public void addSecuritiesListener(ISecurityListener listener) {
        if(!securitiesListeners.contains(listener)) securitiesListeners.add(listener);
    }

    public void addMarketDataListener(IMarketDataListener listener) {
        if(!marketDataListeners.contains(listener)) marketDataListeners.add(listener);
    }

    public void addTradingSessionStatusListener(ITradingSessionStatusListener listener) {
        if(!tradingSessionStatusListeners.contains(listener)) tradingSessionStatusListeners.add(listener);
    }

    public void addPositionListener(IPositionListener listener) {
        if(!positionListeners.contains(listener)) positionListeners.add(listener);
    }

    public void addExecutionReportListener(IExecutionReportListener listener) {
        if(!executionReportListeners.contains(listener)) executionReportListeners.add(listener);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public Organization getOrganization() {
        return organization;
    }

    public long getNextId() {
        return idGenerator.incrementAndGet();
    }

    public MarketDataModel getMarketDataModel() {
        return marketDataModel;
    }

    public OrganizationsModel getOrganizationsModel() {
        return organizationsModel;
    }

    public RoutesModel getRoutesModel() {
        return routesModel;
    }

    public SessionsManager getSessionsManager() {
        return sessionsManager;
    }

    public void init() throws Exception {
        if(config == null) throw new Exception("Configuration is not provided");


        apiClient = new ApiClient();
        apiClient.setConfig(config);
        apiClient.init();

        marketDataModel = new MarketDataModel();
        marketDataModel.setApiClient(apiClient);
        marketDataModel.setConfig(config);
        marketDataModel.init();

        organizationsModel = new OrganizationsModel();
        organizationsModel.setApiClient(apiClient);
        organizationsModel.setConfig(config);
        organizationsModel.init();

        routesModel = new RoutesModel();
        routesModel.setApiClient(apiClient);
        routesModel.setConfig(config);
        routesModel.init();

        organization = organizationsModel.getOrganization();

        addConnectionStatusListener(event -> {
            if(event.getSessionId() == 2 && event.getStatus() == ConnectionStatusEvent.Status.CONNECTED) {
                List<Login> logins = new ArrayList<>();
                for(Account account : getOrganizationsModel().getAccounts()) {
                    List<Login>accountLogins = getOrganizationsModel().getLogins(account.getId());
                    if(accountLogins != null) {
                        logins.addAll(accountLogins);
                    } else {
                        System.out.println("Can't find logins for Account '" + account.getName() + "'");
                    }
                }
                sessionsManager.send(new PositionsRequest(
                        getNextId(),
                        event.getSessionId(),
                        getOrganization(),
                        logins
                ));
                sessionsManager.send(new TradingSessionStatusRequest(
                        getNextId(),
                        event.getSessionId()
                ));
            }
        });

        initSessionsManager();
    }

    private void initSessionsManager() throws Exception {
        sessionsManager = new SessionsManager();
        sessionsManager.setConfig(config);
        sessionsManager.setMarketDataModel(marketDataModel);
        sessionsManager.setConnectionStatusListener(event -> connectionStatusListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.setSecuritiesListener(event -> securitiesListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.setMarketDataListener(event -> marketDataListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.setTradingSessionStatusListener(event -> tradingSessionStatusListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.setPositionListener(event -> positionListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.setExecutionReportListener(event -> executionReportListeners.forEach(listener -> listener.onEvent(event)));
        sessionsManager.init();
    }

    public void setConfig(Properties config) {
        this.config = config;
    }
}
