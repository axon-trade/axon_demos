package io.xtrd.samples.sts.gui.management.data;


import io.xtrd.samples.common.org.Organization;

public class OrganizationEntity extends BaseEntity<Organization> {
    public OrganizationEntity(Organization org) {
        super(org);
        name.setValue(org.getName());
    }
}
