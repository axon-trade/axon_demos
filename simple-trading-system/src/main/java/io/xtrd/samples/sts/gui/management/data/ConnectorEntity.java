package io.xtrd.samples.sts.gui.management.data;

import io.xtrd.samples.common.Connector;

public class ConnectorEntity extends BaseEntity<Connector> {
    public ConnectorEntity(Connector connector) {
        super(connector);
        name.setValue(connector.getName());
    }
}
