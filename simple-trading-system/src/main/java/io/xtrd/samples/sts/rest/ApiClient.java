package io.xtrd.samples.sts.rest;

import io.xtrd.samples.common.Connector;
import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.org.Login;
import io.xtrd.samples.common.org.Organization;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.common.Command;
import io.xtrd.samples.sts.rest.commands.*;
import javafx.util.Pair;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Result;
import org.eclipse.jetty.client.util.BufferingResponseListener;
import org.eclipse.jetty.client.util.StringContentProvider;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ApiClient {
    enum RequestType {GET, POST, PUT, DELETE}
    private final Logger logger = LoggerFactory.getLogger(ApiClient.class);
    private Properties config;
    private HttpClient httpClient;
    private String baseApiUrl;
    private String apiToken;
    private String organizationId;
    private MessagesNormalizer messagesNormalizer;
    private MessagesFactory messagesFactory;

    public void init() throws Exception {
        httpClient = new HttpClient(new SslContextFactory(true));
        httpClient.start();

        baseApiUrl = config.getProperty(Constants.Settings.BASE_API_URL);
        apiToken = config.getProperty(Constants.Settings.API_TOKEN);
        organizationId = config.getProperty(Constants.Settings.ORGANIZATION_ID);

        messagesNormalizer = new MessagesNormalizer();
        messagesFactory = new MessagesFactory();
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

    public void addAccount(long requestId, Account account, BiConsumer<Account, Login>consumer) {
        Request request = createRequest(RequestType.POST, "/rest/organizations/" + organizationId + "/accounts");
        request.content(new StringContentProvider(messagesFactory.getAddAccount(account)));

        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new AddAccount(requestId, account, consumer), result, getContentAsString());
            }
        });
    }

    public void getOrganization(long requestId, int organizationId, Consumer<Organization>consumer) {
        Request request = createRequest(RequestType.GET, "/rest/organizations/" + organizationId);
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetOrganization(requestId, consumer), result, getContentAsString());
            }
        });
    }

    public void deleteAccount(long requestId, Account account, Consumer<Boolean>consumer) {
        Request request = createRequest(RequestType.POST, "/rest/organizations/" + organizationId + "/accounts/" + account.getId());
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new DeleteAccount(requestId, consumer), result, getContentAsString());
            }
        });
    }

    public void changeAccountProperties(long requestId, Account account, List<Pair<String, String>> pairs, Consumer<Boolean>consumer) {
        Request request = createRequest(RequestType.POST, "/rest/organizations/" + organizationId + "/accounts/" + account.getId() + "/properties");
        request.content(new StringContentProvider(messagesFactory.getProperties(pairs)));

        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new ChangeProperty(requestId, consumer), result, getContentAsString());
            }
        });

    }

    public void getAccounts(long requestId, Consumer<List<Account>> consumer) {
        Request request = createRequest(RequestType.GET, "/rest/organizations/" + organizationId + "/accounts/statuses");
        request.param("statusId", "1");
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetAccounts(requestId, consumer), result, getContentAsString());
            }
        });
    }

    public void getLogins(long requestId, Account account, Consumer<List<Login>>consumer) {
        Request request = createRequest(RequestType.GET, "/rest/organizations/" + account.getOrganizationId() + "/accounts/" + account.getId() + "/logins/statuses");
        request.param("statusId", "1");
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetLogins(requestId, account, consumer), result, getContentAsString());
            }
        });
    }

    public void getConnectors(long requestId, Account account, Consumer<List<Connector>>consumer) {
        String targetUrl = "/rest/organizations/" + account.getOrganizationId() + "/exchanges";

        Request request = createRequest(RequestType.GET, targetUrl);
        request.param("ACCOUNT_ID", String.valueOf(account.getId()));
        if(logger.isDebugEnabled()) logger.debug("Sending Request ID: {} to {}", requestId, targetUrl + request.getParams());
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetConnectors(requestId, account, consumer), result, getContentAsString());
            }
        });
    }

    public void getRoutes(long requestId, int routerId, Consumer<List<Exchange>>consumer) {
        Request request = createRequest(RequestType.GET, "/rest/routes");
        request.param("router_id", "" + routerId);
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetRoutes(requestId, consumer), result, getContentAsString());
            }
        });
    }

    public void getRouteSettings(long requestId, int routeId, Consumer<List<Pair<String, String>>>consumer) {
        Request request = createRequest(RequestType.GET, "/rest/routes/" + routeId);
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
                processServerResponse(new GetRouteSettings(requestId, consumer), result, getContentAsString());
            }
        });

    }

    public void addConnector(long requestId, Account account, Exchange exchange, List<Pair<String, String>>settings, Consumer<Connector>consumer) {
        Request request = createRequest(RequestType.POST, "/rest/organizations/" + account.getOrganizationId() + "/accounts/" + account.getId() + "/exchanges/add");
        request.content(new StringContentProvider(messagesFactory.addConnector(exchange, settings)));
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
            processServerResponse(new AddConnector(requestId, account, exchange, settings, consumer), result, getContentAsString());
            }
        });
    }

    public void getExchanges(long requestId, Consumer<List<Exchange>>consumer) {
        Request request = createRequest(RequestType.GET, "/rest/exchanges");
        request.send(new BufferingResponseListener() {
            @Override
            public void onComplete(Result result) {
            processServerResponse(new GetExchanges(requestId, consumer), result, getContentAsString());
            }
        });
    }

    private void processServerResponse(Command cmd, Result result, String messageBody) {
        if(logger.isDebugEnabled()) logger.debug("Received response for Command {}. Success: {}, Status code: {}, Body: {}", cmd, result.isSucceeded(), result.getResponse().getStatus(), messageBody);
        messagesNormalizer.parse(cmd, messageBody);
    }

    private Request createRequest(RequestType type, String url) {
        Request result;
        if (type == RequestType.GET) {
            result =  httpClient.newRequest(baseApiUrl + url);
        } else if (type == RequestType.POST) {
            result = httpClient.POST(baseApiUrl + url);
        } else if (type == RequestType.DELETE) {
            result = httpClient.newRequest(baseApiUrl + url);
            result.method(HttpMethod.DELETE);
        } else if (type == RequestType.PUT) {
            result = httpClient.newRequest(baseApiUrl + url);
            result.method(HttpMethod.PUT);
        } else {
            throw new RuntimeException("Unsupported request type");
        }

        result
            .header(Constants.Headers.API_KEY, apiToken)
            .header(HttpHeader.CONTENT_TYPE, MimeTypes.Type.APPLICATION_JSON.toString());

        return result;
    }

}
