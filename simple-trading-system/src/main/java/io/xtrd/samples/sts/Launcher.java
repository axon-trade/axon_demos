package io.xtrd.samples.sts;

import biz.onixs.fix.engine.Engine;
import biz.onixs.util.settings.PropertyBasedSettings;
import com.airhacks.afterburner.injection.Injector;
import io.xtrd.samples.sts.gui.DashboardView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Launcher extends Application {
    private static String configName;
    private Properties config;
    private Core core;

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(configName);

        Map<Object, Object> complexProperties = new HashMap<>();
        complexProperties.put("core", core);
        Injector.setConfigurationSource(complexProperties::get);

        DashboardView dashboard = new DashboardView();
        Scene scene = new Scene(dashboard.getView());

        primaryStage.setTitle("XTRD | Simple Trading System v 1.0");
        primaryStage.getIcons().add(new Image(Launcher.class.getResourceAsStream("/logo.png")));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void init(String configName) throws Exception {
        config = new Properties();
        config.load(new FileReader(configName));


        initFixEngine();

        core = new Core();
        core.setConfig(config);
        core.init();
    }

    private void initFixEngine() {
        String fixEngineSettingsFile = config.getProperty(Constants.Settings.FIX_ENGINE_CONFIG);
        PropertyBasedSettings settings = new PropertyBasedSettings(fixEngineSettingsFile);
        Engine.init(settings);
    }

    public static void main(String []args) throws Exception {
        if(args.length == 0) throw new IllegalArgumentException("No config provided");
        configName = args[0];

        launch(args);
    }
}
