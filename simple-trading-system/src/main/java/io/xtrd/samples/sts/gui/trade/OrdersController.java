package io.xtrd.samples.sts.gui.trade;

import io.xtrd.samples.common.org.Account;
import io.xtrd.samples.common.trade.ExecutionReport;
import io.xtrd.samples.common.trade.Order;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.fix.commads.NewOrderSingle;
import io.xtrd.samples.sts.fix.commads.OrderCancelReplaceRequest;
import io.xtrd.samples.sts.fix.commads.OrderCancelRequest;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class OrdersController implements Initializable {
    @Inject private Core core;
    @FXML TableView<OrderRow> ordersTable;
    @FXML Button orderDialogButton;
    @FXML Button cancelAllOrdersButton;
    private int idGenerator = 1;
    private Map<Integer, OrderRow> oms = new HashMap<>();
    private ContextMenu contextMenu;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initContextMenu();
        core.addExecutionReportListener(this::onExecutionReport);
        orderDialogButton.setOnAction(this::onOpenOrder);
        cancelAllOrdersButton.setOnAction(this::onCancelAllOrders);

        ordersTable.setRowFactory(param -> {
            TableRow<OrderRow> row = new TableRow<>();
            row.contextMenuProperty().bind(Bindings.when(Bindings.isNotNull(row.itemProperty())).then(contextMenu).otherwise((ContextMenu) null));

            return row;
        });
    }

    private void initContextMenu() {
        contextMenu = new ContextMenu();
        MenuItem updateItem = new MenuItem("Update");
        updateItem.setOnAction(this::onOrderUpdate);

        MenuItem cancelItem = new MenuItem("Cancel");
        cancelItem.setOnAction(this::onOrderCancel);

        contextMenu.getItems().addAll(updateItem, cancelItem);
    }

    private void onOpenOrder(ActionEvent event) {
        OrderView dialog = new OrderView();
        Scene scene = new Scene(dialog.getView());
        Stage stage = new Stage();
        stage.initModality(Modality.NONE);
        stage.setScene(scene);
        stage.setTitle("Open Order");
        stage.showAndWait();

        OrderController controller = (OrderController) dialog.getPresenter();
        if(controller.getButtonType() == ButtonType.OK) {
            Order order = controller.getOrder();
            order.setId(idGenerator++);

            Account account = core.getOrganizationsModel().getAccount(core.getOrganizationsModel().getLogin(order.getLoginId()).getAccountId());

            OrderRow row = new OrderRow(account, order);
            oms.put(order.getId(), row);
            ordersTable.getItems().add(row);

            core.getSessionsManager().send(new NewOrderSingle(core.getNextId(), 2, order));
        }
    }

    private void onOrderUpdate(ActionEvent event) {
        OrderRow row = ordersTable.getSelectionModel().getSelectedItem();

        Order order = row.getOrder();

        UpdateView dialog = new UpdateView();
        Scene scene = new Scene(dialog.getView());
        Stage stage = new Stage();
        stage.initModality(Modality.NONE);
        stage.setScene(scene);
        stage.setTitle("Update Order ID " + order.getId() + "/" + order.getOrderId());

        Order updateOrder = order.copy();
        updateOrder.setExecType(Order.ExecType.REPLACE);

        UpdateController controller = (UpdateController) dialog.getPresenter();
        controller.setOrder(updateOrder);

        stage.showAndWait();

        if(controller.getButtonType() == ButtonType.OK) {
            updateOrder = controller.getOrder();
            updateOrder.setOriginalId(order.getId());
            updateOrder.setId(idGenerator++);

            Account account = core.getOrganizationsModel().getAccount(core.getOrganizationsModel().getLogin(order.getLoginId()).getAccountId());

            OrderRow updateOrderRow = new OrderRow(account, updateOrder);
            oms.put(updateOrder.getId(), updateOrderRow);
            //ordersTable.getItems().add(row);

            core.getSessionsManager().send(new OrderCancelReplaceRequest(core.getNextId(), 2, updateOrder));
        }


    }

    private void onOrderCancel(ActionEvent event) {
        OrderRow row = ordersTable.getSelectionModel().getSelectedItem();
        cancelOrder(row);
    }

    private void cancelOrder(OrderRow row) {
        Order cancelOrder = row.getOrder().copy();
        cancelOrder.setId(idGenerator++);
        cancelOrder.setOriginalId(row.getOrder().getId());
        cancelOrder.setExecType(Order.ExecType.CANCEL);


        Account account = core.getOrganizationsModel().getAccount(core.getOrganizationsModel().getLogin(cancelOrder.getLoginId()).getAccountId());
        OrderRow cancelOrderRow = new OrderRow(account, cancelOrder);
        oms.put(cancelOrder.getId(), cancelOrderRow);

        core.getSessionsManager().send(new OrderCancelRequest(core.getNextId(), 2, cancelOrder));
    }

    private void onCancelAllOrders(ActionEvent event) {
        List<OrderRow> pendingOrders = oms.values().stream().filter(row -> {
            switch (row.getOrder().getExecType()) {
                case TRADE:
                    switch (row.statusProperty().get()) {
                        case NEW:
                        case PARTIALLY_FILLED:
                            return true;
                        default:
                            return false;
                    }
                default:
                    return false;
            }
        }).collect(Collectors.toList());

        for(OrderRow row : pendingOrders) {
            cancelOrder(row);
        }
    }

    private void onExecutionReport(ExecutionReport report) {
        OrderRow row = oms.get(report.getId());
        if(row != null) {
            switch (row.getOrder().getExecType()) {
                case TRADE:
                    boolean finished = row.update(report);
                    if(finished) {
                        oms.remove(report.getId());
                        ordersTable.getItems().remove(row);
                    }
                    break;
                case CANCEL:
                    switch (report.getStatus()) {
                        case CANCELED:
                            oms.remove(report.getId());
                            OrderRow parentOrder = oms.remove(row.getOrder().getOriginalId());
                            if(parentOrder != null) {
                                ordersTable.getItems().remove(parentOrder);
                            }
                            break;
                        case REJECTED:
                            oms.remove(report.getId());
                            break;
                    }
            }
        }
    }
}
