package io.xtrd.samples.sts.fix;

import biz.onixs.fix.engine.Session;
import biz.onixs.fix.engine.SessionState;
import biz.onixs.fix.parser.Group;
import biz.onixs.fix.parser.Message;
import biz.onixs.fix.tag.FIX44;
import biz.onixs.fix.tag.Tag;
import io.xtrd.samples.common.fix.*;
import io.xtrd.samples.sts.md.MarketDataModel;

public class Listener implements Session.InboundApplicationMessageListener, Session.StateChangeListener {
    private int sessionId;
    private IConnectionStatusListener connectionStatusListener = null;
    private ISecurityListener securitiesListener = null;
    private IMarketDataListener marketDataListener = null;
    private ITradingSessionStatusListener tradingSessionStatusListener = null;
    private IPositionListener positionListener = null;
    private IExecutionReportListener executionReportListener = null;
    private MessagesNormalizer messagesNormalizer;

    public Listener(int sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public void onInboundApplicationMessage(Object o, Session.InboundApplicationMessageArgs args) {
        switch (args.getMsg().getType()) {
            case FIX44.MsgType.SecurityList:
                processSecurityList(args.getMsg());
                break;
            case FIX44.MsgType.MarketDataSnapshotFullRefresh:
                processMarketDataSnapshot(args.getMsg());
                break;
            case FIX44.MsgType.MarketDataIncrementalRefresh:
                processMarketDataIncrementalRefresh(args.getMsg());
                break;
            case FIX44.MsgType.TradingSessionStatus:
                processTradingSessionStatus(args.getMsg());
                break;
            case FIX44.MsgType.PositionReport:
                processPositionReport(args.getMsg());
                break;
            case FIX44.MsgType.ExecutionReport:
                processExecutionReport(args.getMsg());
                break;
        }
    }

    @Override
    public void onStateChange(Object o, Session.StateChangeArgs args) {
        if(args.getNewState() == SessionState.ESTABLISHED) {
            connectionStatusListener.onEvent(new ConnectionStatusEvent(sessionId, ConnectionStatusEvent.Status.CONNECTED));
        } else if (args.getNewState() == SessionState.DISCONNECTED) {
            connectionStatusListener.onEvent(new ConnectionStatusEvent(sessionId, ConnectionStatusEvent.Status.DISCONNECTED));
        } else { }
    }

    public void setConnectionStatusListener(IConnectionStatusListener connectionStatusListener) {
        this.connectionStatusListener = connectionStatusListener;
    }

    public void setSecuritiesListener(ISecurityListener securitiesListener) {
        this.securitiesListener = securitiesListener;
    }

    public void setMarketDataListener(IMarketDataListener marketDataListener) {
        this.marketDataListener = marketDataListener;
    }

    public void setTradingSessionStatusListener(ITradingSessionStatusListener tradingSessionStatusListener) {
        this.tradingSessionStatusListener = tradingSessionStatusListener;
    }

    public void setPositionListener(IPositionListener positionListener) {
        this.positionListener = positionListener;
    }

    public void setExecutionReportListener(IExecutionReportListener executionReportListener) {
        this.executionReportListener = executionReportListener;
    }

    private void processSecurityList(Message msg) {
        securitiesListener.onEvent(messagesNormalizer.parseSecurityList(msg));
    }

    private void processMarketDataSnapshot(Message msg) {
        marketDataListener.onEvent(messagesNormalizer.parseMarketDataSnapshot(msg));
    }

    private void processMarketDataIncrementalRefresh(Message msg) {
        marketDataListener.onEvent(messagesNormalizer.parseMarketDataIncrementalRefresh(msg));
    }

    private void processTradingSessionStatus(Message msg) {
        tradingSessionStatusListener.onEvent(messagesNormalizer.parseTradingSessionStatus(msg));
    }

    private void processPositionReport(Message msg) {
        positionListener.onEvent(messagesNormalizer.parsePositionReport(msg));
    }

    private void processExecutionReport(Message msg) {
        executionReportListener.onEvent(messagesNormalizer.parseExecutionReport(msg));
    }

    public void setMessagesNormalizer(MessagesNormalizer messagesNormalizer) {
        this.messagesNormalizer = messagesNormalizer;
    }
}
