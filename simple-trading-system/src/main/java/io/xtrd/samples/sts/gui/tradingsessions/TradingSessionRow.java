package io.xtrd.samples.sts.gui.tradingsessions;

import io.xtrd.samples.common.fix.TradingSessionStatusEvent;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class TradingSessionRow {
    private SimpleStringProperty account;
    private SimpleStringProperty exchange;
    private SimpleObjectProperty<TradingSessionStatusEvent.Status>status;
    private SimpleStringProperty text;

    public TradingSessionRow(TradingSessionStatusEvent event) {
        account = new SimpleStringProperty(event.getAccount().getName());
        exchange = new SimpleStringProperty(event.getExchange().getName());
        status = new SimpleObjectProperty<>(event.getStatus());
        text = new SimpleStringProperty(event.getText());
    }

    public SimpleStringProperty accountProperty() {
        return account;
    }

    public SimpleStringProperty exchangeProperty() {
        return exchange;
    }

    public SimpleObjectProperty<TradingSessionStatusEvent.Status>statusProperty() {
        return status;
    }

    public SimpleStringProperty textProperty() {
        return text;
    }

    public void update(TradingSessionStatusEvent event) {
        status.set(event.getStatus());
    }
}
