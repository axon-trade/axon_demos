package io.xtrd.samples.sts.md;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.common.md.Symbol;
import io.xtrd.samples.sts.Constants;
import io.xtrd.samples.sts.rest.ApiClient;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MarketDataModel {
    private Properties config;
    private ApiClient apiClient;
    private List<Exchange> exchanges = new ArrayList<>();
    private Map<String, Symbol>symbolsCacheByName = new HashMap<>();
    private Map<Integer, Symbol>symbolsMap = new HashMap<>();
    private Map<String, List<Symbol>>exchangesSymbols = new HashMap<>();
    private int idGenerator = 1;
    private long syncTimeout;

    public List<Exchange>getExchanges() {
        return exchanges;
    }

    private String getKey(Symbol symbol) {
        return getKey(symbol.getExchange().getName(), symbol.getName());
    }

    private String getKey(String exchangeName, String symbolName) {
        return exchangeName + ":" + symbolName;
    }

    public List<Symbol>getSymbols(Exchange exchange) {
        List<Symbol>result = exchangesSymbols.get(exchange.getName());
        if(result == null) {
            result = new ArrayList<>();
        }

        return result;
    }

    public Symbol addSymbol(Symbol symbol) {
        Symbol cachedSymbol = symbolsCacheByName.get(getKey(symbol));
        if(cachedSymbol == null) {
            symbol.setId(idGenerator++);
            symbolsCacheByName.put(getKey(symbol), symbol);
            symbolsMap.put(symbol.getId(), symbol);

            cachedSymbol = symbol;

            List<Symbol> exchangeSymbols = exchangesSymbols.computeIfAbsent(symbol.getExchange().getName(), k -> new ArrayList<>());
            if(!exchangeSymbols.contains(symbol)) exchangeSymbols.add(cachedSymbol);
        }

        return cachedSymbol;
    }

    public Symbol getSymbol(String exchangeName, String symbolName) {
        return symbolsCacheByName.get(getKey(exchangeName, symbolName));
    }

    public void init()throws Exception {
        initExchanges();
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

    private void initExchanges() throws Exception {
        syncTimeout = Long.parseLong(config.getProperty(Constants.Settings.SYNC_TIMEOUT, "5000"));
        CountDownLatch latch = new CountDownLatch(1);
        apiClient.getExchanges(1, ack -> {
            exchanges.addAll(ack);
            latch.countDown();
        });

        if(!latch.await(syncTimeout, TimeUnit.MILLISECONDS)) throw new Exception("Timeout during fetching exchanges");
    }

}
