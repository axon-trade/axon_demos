package io.xtrd.samples.sts.gui.management;

import io.xtrd.samples.common.md.Exchange;
import io.xtrd.samples.sts.Core;
import io.xtrd.samples.sts.gui.utils.FieldsHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Pair;

import javax.inject.Inject;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ConnectorController implements Initializable {
    @Inject private Core core;
    @FXML ChoiceBox<Exchange>routeSelector;
    @FXML TableView<SettingRow> settingsTable;
    @FXML TableColumn<SettingRow, String> valueColumn;
    @FXML Button addButton;
    @FXML Button cancelButton;


    public List<Pair<String, String>> getSettings() {
        ArrayList<Pair<String, String>>result = new ArrayList<>();
        settingsTable.getItems().forEach(pair -> result.add(pair.getPair()));

        return result;
    }

    public Exchange getExchange() {
        return routeSelector.getValue();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        routeSelector.setConverter(FieldsHelper.getStringConverter());
        routeSelector.getItems().addAll(core.getRoutesModel().getRoutes());

        routeSelector.getSelectionModel().selectedItemProperty().addListener((obs, old, newValue) -> {
            settingsTable.getItems().clear();
            core.getRoutesModel().getSettings(newValue.getId()).forEach(pair -> settingsTable.getItems().add(new SettingRow(pair)));
        });

        valueColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        valueColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).valueProperty().set(event.getNewValue());
        });

        cancelButton.setOnAction(e -> {
            closeWindow((Node) e.getSource());
        });

        addButton.setOnAction(e -> {
            closeWindow((Node) e.getSource());
        });
    }

    private void closeWindow(Node source) {
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();

    }
}
